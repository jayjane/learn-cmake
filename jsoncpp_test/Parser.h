#ifndef MY_PARSER_H
#define MY_PARSER_H

#include <fstream>
#include <jsoncpp/json/json.h>
#include <vector>

class MyParser {
public:
  MyParser() = default;
  ~MyParser() = default;

  inline static Json::Value parse(const char *inputFile) {
    Json::Value val;
    std::ifstream ifs(inputFile);
    Json::Reader reader;
    reader.parse(ifs, val);
    return val;
  }
};

#endif /* MY_PARSER_H */