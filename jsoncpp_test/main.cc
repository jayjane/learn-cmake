#include "Parser.h"
#include <iostream>

int main() {
  Json::Value val = MyParser::parse("../input.json");
  std::cout << val["name"] << std::endl;
}