#!/bin/sh

# Display usage
cpack_usage()
{
  cat <<EOF
Usage: $0 [options]
Options: [defaults in brackets after descriptions]
  --help            print this message
  --version         print cmake installer version
  --prefix=dir      directory in which to install
  --include-subdir  include the Demo8-1.0.1-Linux subdirectory
  --exclude-subdir  exclude the Demo8-1.0.1-Linux subdirectory
  --skip-license    accept license
EOF
  exit 1
}

cpack_echo_exit()
{
  echo $1
  exit 1
}

# Display version
cpack_version()
{
  echo "Demo8 Installer Version: 1.0.1, Copyright (c) Humanity"
}

# Helper function to fix windows paths.
cpack_fix_slashes ()
{
  echo "$1" | sed 's/\\/\//g'
}

interactive=TRUE
cpack_skip_license=FALSE
cpack_include_subdir=""
for a in "$@"; do
  if echo $a | grep "^--prefix=" > /dev/null 2> /dev/null; then
    cpack_prefix_dir=`echo $a | sed "s/^--prefix=//"`
    cpack_prefix_dir=`cpack_fix_slashes "${cpack_prefix_dir}"`
  fi
  if echo $a | grep "^--help" > /dev/null 2> /dev/null; then
    cpack_usage 
  fi
  if echo $a | grep "^--version" > /dev/null 2> /dev/null; then
    cpack_version 
    exit 2
  fi
  if echo $a | grep "^--include-subdir" > /dev/null 2> /dev/null; then
    cpack_include_subdir=TRUE
  fi
  if echo $a | grep "^--exclude-subdir" > /dev/null 2> /dev/null; then
    cpack_include_subdir=FALSE
  fi
  if echo $a | grep "^--skip-license" > /dev/null 2> /dev/null; then
    cpack_skip_license=TRUE
  fi
done

if [ "x${cpack_include_subdir}x" != "xx" -o "x${cpack_skip_license}x" = "xTRUEx" ]
then
  interactive=FALSE
fi

cpack_version
echo "This is a self-extracting archive."
toplevel="`pwd`"
if [ "x${cpack_prefix_dir}x" != "xx" ]
then
  toplevel="${cpack_prefix_dir}"
fi

echo "The archive will be extracted to: ${toplevel}"

if [ "x${interactive}x" = "xTRUEx" ]
then
  echo ""
  echo "If you want to stop extracting, please press <ctrl-C>."

  if [ "x${cpack_skip_license}x" != "xTRUEx" ]
  then
    more << '____cpack__here_doc____'
The MIT License (MIT)

Copyright (c) 2013 Joseph Pan(http://hahack.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

____cpack__here_doc____
    echo
    echo "Do you accept the license? [yN]: "
    read line leftover
    case ${line} in
      y* | Y*)
        cpack_license_accepted=TRUE;;
      *)
        echo "License not accepted. Exiting ..."
        exit 1;;
    esac
  fi

  if [ "x${cpack_include_subdir}x" = "xx" ]
  then
    echo "By default the Demo8 will be installed in:"
    echo "  \"${toplevel}/Demo8-1.0.1-Linux\""
    echo "Do you want to include the subdirectory Demo8-1.0.1-Linux?"
    echo "Saying no will install in: \"${toplevel}\" [Yn]: "
    read line leftover
    cpack_include_subdir=TRUE
    case ${line} in
      n* | N*)
        cpack_include_subdir=FALSE
    esac
  fi
fi

if [ "x${cpack_include_subdir}x" = "xTRUEx" ]
then
  toplevel="${toplevel}/Demo8-1.0.1-Linux"
  mkdir -p "${toplevel}"
fi
echo
echo "Using target directory: ${toplevel}"
echo "Extracting, please wait..."
echo ""

# take the archive portion of this file and pipe it to tar
# the NUMERIC parameter in this command should be one more
# than the number of lines in this header file
# there are tails which don't understand the "-n" argument, e.g. on SunOS
# OTOH there are tails which complain when not using the "-n" argument (e.g. GNU)
# so at first try to tail some file to see if tail fails if used with "-n"
# if so, don't use "-n"
use_new_tail_syntax="-n"
tail $use_new_tail_syntax +1 "$0" > /dev/null 2> /dev/null || use_new_tail_syntax=""

extractor="pax -r"
command -v pax > /dev/null 2> /dev/null || extractor="tar xf -"

tail $use_new_tail_syntax +167 "$0" | gunzip | (cd "${toplevel}" && ${extractor}) || cpack_echo_exit "Problem unpacking the Demo8-1.0.1-Linux"

echo "Unpacking finished successfully"

exit 0
#-----------------------------------------------------------
#      Start of TAR.GZ file
#-----------------------------------------------------------;

� �"�c �\}l[����ḭ㶁(�+%S��v��RJ��N_X��д��}u����ؙ�LDG� ���E6��0�?�GѴ� ���Q@ 6&�R&�����z��w����3i$Z��Nd��=�~ߗ�u�@4ބ.19�ζ��pL,W�����t���ݜ��ju��r�3F(��I��d"!}��L�l�#4���8���i|s�{�W��]'n��ن8����y�����*3��r�i��e/�/�*�xQ;����E�Q%��Uz^���H�p��� �ʼ��(7�x��^�W!G��]�JFP>ʯ5#W���q�s^?�\mG�&ۨ��5^1�B���n�A�rD���,����P���u��N�OhӇR����B�(�	�nb�n�v���V{�W�^� ���jm�EZ=M�Pc,O�j������q����Z���������o��u���Co?~�W�|�rȃ	tѺ���
0�������jKy���ʑ�`V���?wVw�������7�n�{�ݳ�����o����w��������6��������1Y�S�^��gA���tp_	��%�%��Crڋ��j��d�mb�#����B$����ӝ��PA�nk����GA���M7���8࿗�9�q}�B��+%�6T�P��4�w@~�i �))\����V$�C��@��H�@B�� �k0RDr)���i)���ѸF)!Eq4D�
D�	��B8��qp(�)1��C)��u�*�R)����N1�Q�ql(C�����Sp;܎��2�3��#Dx*�����jb���k���T�1�:h�*T�8��oP�f�T����*�B�{U�l�cd�Ad�A�u��>��{*�6�LHٕ2��eR	ϵ��A�����f��"r��:��T������1Y~�	V��&d�"��ک���<����~Y~��djvj���ݙ׷���ɩM���N��9�Y �G��y}�)�x>\�/f�	��|K�%�Ũ>L�����d8�Yr�\�������Xf�)���k�#�f��"��i~>�*���]s�/M/��3f�3�b�#�m�r~X�3��"�egf�{|&}ll�{e�u�{���=���,�<>��a;^�����B.*$�|�npH��1bDT�&,����~�k~>�g��f��GpX���G�݊�=���t��">�m��Q���?
ᚆ�2o�J�H>H�f?�����IR�s5����Ṛ&2���v�Di+F*�6�ά<�&�Q���ܽ�\�
�W�j�(���3����0�C�V_&��ҝ���ߝyӷ�go���~~o#��,���.�k3���F�/�����}O�tO��u8�\�{�ؤ�_u&}�t�l��Էͷ�'L���?�0&
C� �2� �2� �2���L����S�1��&�\}�Q���T`P�����ȉ��q1.qVY�7���)ri�H'���8�! E�Xt H�88T?�m�qq�W?hզ>��t�y59[&g0�?����ǩ\�v�O`~��?��aΝ��0Ǽ�ji9��C�]v�5s�,�*;��݁�8�������X�(��]��[j�촌��W�^�|�ut��
�������uΫ,{Ǥ��3��8�_�I'�d�ـ�$�ev��)������6no�����
��|�����Um�yc�v���5tظ[��Y�s�W�g�Gۂd�Ad�Ad���v�}H'p�}j5_�����\1ܧ�"�/y��^�5 ���W����L�gr�O�Ŭ�ӻ{|�"�;{�� ���HK���p����i9��k䋀�Vz5xd�o�^5#_�)��A?2��)�C����r�Z�]��o~7�U�i�wv��5���_�y���t�8[��\C��������r��*G�Å������,���h2�Z�~Z�,�]��QV/��7-^��gZ�2��x�n;�q/�u��|?���|�����|.�t����
^|�Y�����=����	]|A~�k��q��ku���^zX�_��Z��޹��W�0�������^��	�>m�O1�R��s�R��F!?�9�%�.��!�gψ�_\Ϗ����x��>-��G�.��A������~�Oq{ee|Q�_�y�A&�UiAQ�, ���z���_�٤��0��Et����1��3����7%��1���J�?\Ҥ�G�'��_ǄI߿�X��O���.>6��E�3���|a��!���I��RJJ�Î 
�Iq0��Ĥ 	�X".��CDB�%1!$%�)!�ޅ����(�!G[��U_I���d`D�Rr���!Q���F��J��+4��W�"�ޕ��h"�"� t��6��:A��(�y �u}HX߳���#l��ͿY�������:�F�TB�⡘�M�!- �����m����W��Q�P��\!~)��m0q^af�B�^��J8���*�Or�F��� �RR��XL#G<!���x�1���B��@���F)0��H A��Hǧp)��ܭ�h�%�X�(¯�D��qH�.�-W�#��3�#�k"�dAR,�S,�oq`(ĩ&$�KI@�Wr��<�{��H����	�kLE^��y�����zF���[{:?���`O�U>�shjO�q��� ��#,HK�HY3P{:ߋ��q(0Yw�P�|R�Ԟ�yH��K(���)kjO珋a����qEw!e��O�y��~�)݃�:͗�#`O��������u�麊�_�c�|fe���*��)��a��<��aF���G{��|~��ٵ"z�����k*r5���?Ҏ�����F}��0�?^E���g��cO��`og:,��R��t]���m�׷0��e֨���n�"��FJ�S���4�ӂU2v��GJ����ޘn���������~�O{:O���m/����E�鼪�"���}��������C �:�x�Ɓ��K̨�&Z^�D��~	���+�����W�ƃ�tH����_ܞ�����A����x8:�\�4p}�z<����Zhg������l���rPS'EDNi�tRq�ay����%�%)Lq�D�ۜ�+�h f]�"E��{��-��ۺ7�
|�l��\%�{q��j�ɅbJNs8�p�C�p2qw4���N.k�֦ܲt���-~a�ƭ܊&��%2h6Dǿv���}�4�]�.�.��݆�?��1���eѰ<�����	|~�R�*��d���@L�b�r�.��B���Z���P4l��Ţ����kik3���H�����vӘ�����ü���0������H#7[�8-�Z�ܞ�����2���87��� �z��>�ڝ'��Y�:>��đh�69�����R��j�1�84�;Y:��c���<4���Ntg��Oij�l�B��a {�t�^����:�%d.�Sb��mQ�P��%��~��:o�Z 
�̿����M�C��H���}�S�S�m�o8f�O�#��=���4ʑn�F��	�f�s�o�H�5{�@^�~��&{t�wzRʿ�d:�ٽ�"f/���eӿi�2gk�B����/��Ki������ٽ��<zMS�Qڊ��x;���'�v�k?�N�YPq���˵@F�@&�e�:���r���Z�ؽ\2�NO��\�2Ƞ����e� `  