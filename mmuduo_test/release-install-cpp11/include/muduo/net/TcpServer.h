#ifndef MUDUO_NET_TCPSERVER_H
#define MUDUO_NET_TCPSERVER_H

#include "muduo/base/Types.h"
#include "muduo/net/TcpConnection.h"

#include <atomic>
#include <map>

namespace muduo {
namespace net {

class Acceptor;
class EventLoop;
class EventLoopThreadPool;

// TCP server, 支持单线程和多线程
// 这是一个接口类，不要暴露太多
class TcpServer : public noncopyable {
public:
  using ThreadInitCallback = std::function<void(EventLoop *)>;

  enum Option {
    kNoReusePort,
    kReusePort,
  };

  TcpServer(EventLoop *loop, const InetAddress &listenAddr,
            const std::string &nameArg, Option option = kNoReusePort);

  ~TcpServer();

  // 返回TCP server的ip:port字符串
  const std::string &ipPort() const { return ipPort_; }
  // 返回TCP server的名字
  const std::string &name() const { return name_; }
  // 返回TCP server所属EventLoop
  EventLoop *getLoop() const { return loop_; }

  void setThreadNum(int numThreads);
  void setThreadInitCallback(const ThreadInitCallback &cb) {
    threadInitCallback_ = cb;
  }

  std::shared_ptr<EventLoopThreadPool> threadPool() { return threadPool_; }

  void start();
  void setConnectionCallback(const ConnectionCallback &cb) {
    connectionCallback_ = cb;
  }

  void setMessageCallback(const MessageCallback &cb) { messageCallback_ = cb; }

  void setWriteCompleteCallback(const WriteCompleteCallback &cb) {
    writeCompleteCallback_ = cb;
  }

private:
  void newConnection(
      int sockfd,
      const InetAddress &peerAddr); // Acceptor拥有的Channel的handleRead调用的
  void removeConneciton(const TcpConnectionPtr &conn);
  void removeConnectionInLoop(const TcpConnectionPtr &conn);

private:
  using ConnectionMap = std::map<std::string, TcpConnectionPtr>;

  EventLoop *loop_; // 注意这个EventLoop是Acceptor所属的EventLoop
  const std::string ipPort_; // 服务ip:port
  const std::string name_;   // 服务名称
  std::unique_ptr<Acceptor> acceptror_;

  /*
  线程池，可以设置线程个数。
  如果为0，则线程池的baseLoop_就是TcpServer::loop_,作为mainReactor,
  此时这个EventLoop兼顾Acceptor(listenfd)和TcpConnection(connfd)；
  若线程池个数不为0，TcpServer::loop_还是作为mainReactor，而线程池的EventLoop作为subReactor
  */
  std::shared_ptr<EventLoopThreadPool> threadPool_;

  // 这个连接回调函数是通过用户定义，并调用setConnectionCallback设置的
  ConnectionCallback connectionCallback_;
  // 这个消息回调函数是通过用户定义，并调用setMessageCallback设置的
  MessageCallback messageCallback_;
  WriteCompleteCallback writeCompleteCallback_;
  ThreadInitCallback threadInitCallback_; // IO线程的初始化回调函数
  std::atomic_bool started_;              // TCP server启动标志

  int nextConnId_; // 下一个连接的Id
  ConnectionMap
      connections_; // 连接列表（key：连接名称，value：TcpConnectionPtr）
};                  // end of class TcpServer

}; // end of namespace net
}; // end of namespace muduo

#endif // MUDUO_NET_TCPSERVER_H