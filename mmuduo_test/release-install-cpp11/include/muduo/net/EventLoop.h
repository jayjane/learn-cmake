#ifndef MUDUO_NET_EVENTLOOP_H
#define MUDUO_NET_EVENTLOOP_H

// #include <algorithm>
#include <atomic>
// #include <poll.h>
// #include <signal.h>
// #include <sys/eventfd.h>
// #include <unistd.h>
#include <functional>
#include <mutex>
#include <vector>

#include <boost/any.hpp>

#include "muduo/base/CurrentThread.h"
#include "muduo/base/noncopyable.h"
#include "muduo/base/Timestamp.h"
// #include "muduo/base/Logging.h"
// #include "muduo/base/Thread.h"
#include "muduo/net/Callbacks.h"
// #include "muduo/net/Channel.h"
#include "muduo/net/TimerId.h"
// 这里不能直接include进来，因为TimerQueue.h
// 有#include "muduo/net/EventLoop.h"，这样就会造成循环引用头文件
// 用向前声明解决
// #include "muduo/net/TimerQueue.h"

namespace muduo {
namespace net {

class Channel;
class Poller;
class TimerQueue;

class EventLoop : public noncopyable {

public:
  using Functor = std::function<void()>;

public:
  EventLoop();
  ~EventLoop();

  // 这个就是事件循环函数
  // 必须在创建EventLoop对象的线程中执行
  void loop();

  void quit();

  Timestamp pollReturnTime() const { return pollReturnTime_; }

  int64_t iteration() const { return iteration_; }

  // 线程安全，立刻唤醒loop，调用cb
  void runInLoop(Functor cb);

  // 线程安全，等待poll结束后，才调用cb（期间可能有多个cb，换言之需要排队）
  void queueInLoop(Functor cb);

  size_t queueSize() const;

  TimerId runAt(Timestamp time, TimerCallback cb);     // 在某一个时刻
  TimerId runAfter(double delay, TimerCallback cb);    // 在某一段时间后
  TimerId runEvery(double interval, TimerCallback cb); // 重复

  void cancel(TimerId timerId);

  void wakeup();
  void updateChannel(Channel *channel);
  void removeChannel(Channel *channel);
  bool hasChannel(Channel *channel);

  void assertInLoopThread() {
    // 如果当前线程不是IO线程（不是拥有该EventLoop对象的线程）
    // 就abort
    if (!isInLoopThread()) {
      abortNotInLoopThread();
    }
  }

  // 判断当前线程是否为IO线程
  bool isInLoopThread() const {
    // 判断EventLoop所属线程的tid与当前线程的tid是否相同
    return threadId_ == CurrentThread::tid();
  }

  bool eventHandling() const { return eventHandling_.load(); }

  // 关于contet
  void setContext(const boost::any &context) { context_ = context; }
  const boost::any &getContex() { return context_; }
  boost::any *getMutableContext() { return &context_; }

  static EventLoop *getEventLoopOfCurrentThread();

private:
  void abortNotInLoopThread();
  void hanldeRead();
  void doPendingFunctors();

  void printActiveChannels() const; // DEBUG

  // 这里存放的是指针，代表Channel的声明周期不归EventLoop管
  using ChannelList = std::vector<Channel *>;

  std::atomic_bool looping_;       // 是否正在事件循环
  std::atomic_bool quit_;          // 是否退出时间循环
  std::atomic_bool eventHandling_; // 是否正在处理就绪事件
  std::atomic_bool
      callingPendingFunctors_; // 是否正在处理那些由queueInLoop函数传入的Functor
  int64_t iteration_;              // EventLoop调用loop的次数
  const pid_t threadId_;           // EventLoop所属线程的tid
  Timestamp pollReturnTime_;       // loop函数中Poller::poll的返回时间
  std::unique_ptr<Poller> poller_; // EventLoop的唯一一个Poller
  std::unique_ptr<TimerQueue> timerQueue_; // EventLoop的定时器队列
  int wakeupFd_;                           //
  std::unique_ptr<Channel>
      wakeupChannel_; // 这个Channel的声明周期是由EventLoop对象控制

  boost::any context_; // 上下文？？？？？？

  ChannelList activeChannels_;
  Channel *currentActiveChannels_;

  mutable std::mutex mtx_;
  std::vector<Functor> pendingFunctors_;
}; // end of class EventLoop

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_EVENTLOOP_H
