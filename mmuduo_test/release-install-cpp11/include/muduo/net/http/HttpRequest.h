// Copyright 2010, Shuo Chen.  All rights reserved.
// http://code.google.com/p/muduo/
#ifndef MUDUO_NET_HTTP_HTTPREQUEST_H
#define MUDUO_NET_HTTP_HTTPREQUEST_H

#include "muduo/base/Timestamp.h"
#include "muduo/base/Types.h"
#include "muduo/base/copyable.h"

#include <assert.h>
#include <stdio.h>
#include <unordered_map>

namespace muduo {
namespace net {

class HttpRequest : public muduo::copyable {
public:
  enum Method { kInvalid, kGet, kPost, kHead, kPut, kDelete };
  enum Version { kUnknown, kHttp10, kHttp11 };

  HttpRequest() : method_(kInvalid), version_(kUnknown) {}

  // getter and setter of HTTP request method (method_)
  bool setMethod(const char *start, const char *end) {
    return setMethod(std::string(start, end));
  }

  bool setMethod(const std::string method);

  Method method() const { return method_; }

  const std::string getMethod() const;

  const char *methodString() const { return getMethod().c_str(); }

  // getter and setter of HTTP version (version_)
  void setVersion(Version v) { version_ = v; }

  Version getVersion() const { return version_; }

  // getter and setter of HTTP request path (path_)
  void setPath(const char *start, const char *end) { path_.assign(start, end); }

  void setPath(const std::string &path) { path_ = path; }

  const std::string &getPath() const { return path_; }

  // getter and setter of HTTP request packets receive time (receiveTime_)
  void setReceiveTime(Timestamp t) { receiveTime_ = t; }

  Timestamp getReceiveTime() const { return receiveTime_; }

  // getter and setter of HTTP request packet's header (headers_)
  void addHeader(const char *start, const char *colon, const char *end);

  void addHeader(const std::string &key, const std::string &value) {
    headers_[key] = value;
  }

  std::string getHeader(const std::string &field) const;

  const std::unordered_map<std::string, std::string> &getAllHeaders() const {
    return headers_;
  }

  // getter and setter of parameters after HTTP request path (parameters_)
  void addParameter(const std::string &key, const std::string &value) {
    parameters_[key] = value;
  }

  const std::string getParameter(const std::string &key) const;

  const std::unordered_map<std::string, std::string> &getAllParameters() const {
    return parameters_;
  }

  // swap content
  void swap(HttpRequest &that);

private:
  Method method_;
  Version version_;
  std::string path_;
  Timestamp receiveTime_;
  std::unordered_map<std::string, std::string> headers_;
  std::unordered_map<std::string, std::string> parameters_;
};

} // namespace net
} // namespace muduo 
/** 
 * 对于HTTP请求报文，需要解析的Content-Type的类型无非这几种
 * 左边是Postman的值，右边是Content-Type的值
 * Body-raw-Text: text/plain
 * Body-raw-JavaScript: application/javascript
 * Body-raw-Json: application/json
 * Body-raw-HTML: text/html
 * Body-raw-XML: application/xml
 * Body-form-data: multipart/form-data(还有boundary作为分界，数据的前后都有)
 * Content-Type: multipart/form-data; boundary=--------------------------228925621487344537277013
 * Body-x-www-form-urlencoded: application/x-www-form-urlencoded
 * */
#endif // MUDUO_NET_HTTP_HTTPREQUEST_H
