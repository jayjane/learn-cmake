#ifndef MUDUO_NET_INETADDRESS_H
#define MUDUO_NET_INETADDRESS_H

#include "muduo/base/copyable.h"
#include "muduo/base/StringPiece.h"

#include <netinet/in.h>

namespace muduo {

namespace net {

namespace sockets {
const struct sockaddr *sockaddr_cast(const struct sockaddr_in6 *addr);
}; // end of namespace sockets

/// @brief sockaddr_in的封装类(Wrapper)
/// POD类
class InetAddress : public copyable {
public:
  // 通过端口号和ip版本号构造
  explicit InetAddress(uint16_t port = 0, bool loopbackOnly = false,
                       bool ipv6 = false);

  // 通过ip地址、端口号、ip版本号
  InetAddress(StringArg ip, uint16_t port, bool ipv6 = false);

  // 通过地址结构体构造
  explicit InetAddress(const struct sockaddr_in &addr) : addr_(addr) {}
  explicit InetAddress(const struct sockaddr_in6 &addr) : addr6_(addr) {}

  // 返回协议族
  sa_family_t family() const {
    // 因为addr_和addr6_的结构，并且使用的是union，所以返回addr_的或者是addr6_的都一样
    return addr_.sin_family;
  }
  std::string toIp() const;
  std::string toIpPort() const;
  uint16_t port() const;

  // default copy/assignment are Okay
  // 从继承copyable可以看出，这个类是值语义的

  const struct sockaddr *getSockAddr() const {
    // Todo：为什么只返回addr6_
    return sockets::sockaddr_cast(&addr6_);
  }
  
  void setSockAddrInet6(const struct sockaddr_in6 &addr) { addr6_ = addr; }

  uint32_t ipv4NetEndian() const;

  // 返回网络字节序的端口号
  uint16_t portNetEndian() const { return addr_.sin_port; }

  static bool resolve(StringArg hostname, InetAddress *result);

  // set IPv6 ScopeID
  void setScopeId(uint32_t scope_id);

private:
  // 地址结构体
  union {
    struct sockaddr_in addr_;
    struct sockaddr_in6 addr6_;
  };
}; // end of class InetAddress

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_INETADDRESS_H
