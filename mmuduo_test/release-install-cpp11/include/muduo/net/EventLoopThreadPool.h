#ifndef MUDUO_NET_EVENTLOOPTHREADPOOL_H
#define MUDUO_NET_EVENTLOOPTHREADPOOL_H

#include "muduo/base/noncopyable.h"
#include "muduo/base/Types.h"

#include <functional>
#include <memory>
#include <vector>

namespace muduo {

namespace net {

class EventLoop;
class EventLoopThread;

class EventLoopThreadPool : public noncopyable {

public:
  using ThreadInitCallback = std::function<void(EventLoop *)>;

  EventLoopThreadPool(EventLoop *baseLoop, const std::string &nameArg);
  ~EventLoopThreadPool();
  void setThreadNum(int numThreads) { numThreads_ = numThreads; }
  void start(const ThreadInitCallback &cb = ThreadInitCallback());

  EventLoop *getNextLoop();
  EventLoop *getLoopForHash(size_t hashCode);

  std::vector<EventLoop *> getAllLoops();

  bool started() const { return started_; }

  const std::string &name() const { return name_; }

private:
  EventLoop *baseLoop_; // IO线程池所属的EventLoop
  std::string name_;    // IO线程池名称
  bool started_;        // IO线程池启动标志
  int numThreads_;      // IO线程数目
  int next_; // 新连接到来，所选择的EventLoop对象小标，选定一个EventLoop，即选定一个EventLoopThread
  std::vector<std::unique_ptr<EventLoopThread>> threads_; // IO线程列表
  std::vector<EventLoop *>
      loops_; // 每个IO线程的EventLoop集合，一个IO线程对应一个EventLoop
};

}; // end of namespace net
}; // end of namespace muduo

#endif // MUDUO_NET_EVENTLOOPTHREADPOOL_H