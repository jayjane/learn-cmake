#ifndef MUDUO_BASE_CURRENTTHREAD_H
#define MUDUO_BASE_CURRENTTHREAD_H

#include "muduo/base/Timestamp.h"
#include "muduo/base/Types.h"

#include <stdio.h>
#include <sys/syscall.h> // syscall
#include <sys/types.h>
#include <time.h>   // nanosleep
#include <unistd.h> // getpid

namespace muduo {

namespace CurrentThread {
// __thread关键字修饰变量，表示每个线程各一份。TLS
// __thread使用规则：只能修饰POD类型
// 这里只是声明，定义在.cc文件
extern __thread int t_cachedTid;
extern __thread char t_tidString[32];
extern __thread int t_tidStringLength;
extern __thread const char *t_threadName;

inline void cacheTid() {
  if (t_cachedTid == 0) {
    t_cachedTid = static_cast<pid_t>(::syscall(SYS_gettid));
    // 调用snprintf将tid以C-style字符串保存
    t_tidStringLength =
        snprintf(t_tidString, sizeof(t_tidString), "%5d ", t_cachedTid);
  }
}

inline int tid() {
  // __builtin_expect((x),0)表示 x 的值为假的可能性更大。
  if (__builtin_expect(t_cachedTid == 0, 0)) {
    cacheTid();
  }
  return t_cachedTid;
}

inline const char *tidString() { return t_tidString; }

inline int tidStringLength() { return t_tidStringLength; }

inline const char *name() { return t_threadName; }

inline bool isMainThread() {
  // 如果当前线程为当前进程的主线程，则其tid与当前进程的pid相同
  return tid() == ::getpid();
}

inline void sleepUsec(int64_t usec) {
  struct timespec ts = {0, 0};
  ts.tv_sec = static_cast<time_t>(usec / Timestamp::kMicroSecondsPerSecond);
  ts.tv_nsec =
      static_cast<long>(usec % Timestamp::kMicroSecondsPerSecond * 1000);
  ::nanosleep(&ts, NULL);
}

}; // end of namespace CurrentThread

}; // end of namespace muduo

#endif // !MUDUO_BASE_CURRENTTHREAD_H