#ifndef MUDUO_BASE_COUNTDOWNLATCH_H
#define MUDUO_BASE_COUNTDOWNLATCH_H

#include "muduo/base/noncopyable.h"
#include <condition_variable>
#include <mutex>

namespace muduo {

class CountDownLatch : public noncopyable {
public:
  explicit CountDownLatch(int count) : mtx_(), cond_(), count_(count) {}

  void wait() {
    std::unique_lock<std::mutex> lock(mtx_);
    while (count_ > 0) {
      cond_.wait(lock);
    }
  }

  void countDown() {
    std::lock_guard<std::mutex> lock(mtx_);
    --count_;
    if (count_ == 0) {
      cond_.notify_all();
    }
  }

  int getCount() {
    std::lock_guard<std::mutex> lock(mtx_);
    return count_;
  }

private:
  std::mutex mtx_;
  std::condition_variable cond_;
  int count_;
}; // end of class CountDownLatch

}; // end of namespace muduo

#endif // !MUDUO_BASE_COUNTDOWNLATCH_H