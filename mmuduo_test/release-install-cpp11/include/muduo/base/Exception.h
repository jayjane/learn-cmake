#ifndef MUODUO_BASE_EXCEPTION_H
#define MUODUO_BASE_EXCEPTION_H

#include "muduo/base/Types.h"
#include <exception>
#include <string>

namespace muduo {

using std::string;

class Exception : public std::exception {
public:
  explicit Exception(string what, bool demangle = false);

  ~Exception() noexcept override = default;

  const char *what() const noexcept override { return message_.c_str(); }

  const char *stackTrace() const noexcept { return stackTrace_.c_str(); }

  string fillStackTrace(bool demangle);

private:
  string message_;
  string stackTrace_;
}; // end of class Exception

}; // namespace muduo

#endif // !MUODUO_BASE_EXCEPTION_H
