#ifndef MUDUO_BASE_THREAD_H
#define MUDUO_BASE_THREAD_H

#include "muduo/base/Atomic.h"
#include "muduo/base/CountDownLatch.h"
#include "muduo/base/Types.h"

#include <functional> // function
#include <memory>     // unique_ptr
#include <string>     // string
#include <thread>     // thread

namespace muduo {

class Thread : public noncopyable {
public:
  using ThreadFunc = std::function<void()>;

  explicit Thread(ThreadFunc func, const std::string &name = std::string());
  ~Thread();

  void start();
  void join();

  bool started() const { return started_; }
  pid_t tid() const { return tid_; }
  const std::string &name() const { return name_; }
  static int numCreated() { return numCreated_.get(); }

private:
  void setDefaultName();

  bool joined_;
  bool started_;
  pid_t tid_;
  ThreadFunc func_;
  CountDownLatch
      latch_; // 这个成员变量的唯一作用就是用于控制tid_生成，使得调用tid时返回的是线程的tid，而不是0
  std::string name_;
  std::unique_ptr<std::thread> upt_;

  static AtomicInt32 numCreated_;
}; // end of class Thread

}; // end of namespace muduo

#endif // !MUDUO_BASE_THREAD_H