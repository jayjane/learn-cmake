#ifndef SRC_HTTP_HTTPSERVER_H
#define SRC_HTTP_HTTPSERVER_H

#include <muduo/net/TcpServer.h>

namespace webserver {
namespace http {

class HttpRequest;
class HttpResponse;

class HttpServer : public muduo::noncopyable {
public:
  using HttpCallback = std::function<void(const HttpRequest &, HttpResponse *)>;

  HttpServer(muduo::net::EventLoop *loop, const muduo::net::InetAddress &listenAddr, const std::string &name,
             const std::string &sourceDir,
             muduo::net::TcpServer::Option option = muduo::net::TcpServer::kNoReusePort);

  muduo::net::EventLoop *getLoop() const { return server_.getLoop(); }

  /// Not thread safe, callback be registered before calling start().
  void setHttpCallback(const HttpCallback &cb) { httpCallback_ = cb; }

  void setThreadNum(int numThreads) { server_.setThreadNum(numThreads); }

  void start();

  const std::string &getSourceDir() const { return sourceDir_; }

private:
  void onConnection(const muduo::net::TcpConnectionPtr &conn);
  void onMessage(const muduo::net::TcpConnectionPtr &conn, muduo::net::Buffer *buf,
                 muduo::Timestamp receiveTime);
  void onRequest(const muduo::net::TcpConnectionPtr &, const HttpRequest &);

  muduo::net::TcpServer server_;
  const std::string sourceDir_;
  HttpCallback httpCallback_;
};

} // namespace http
} // namespace webserver

#endif // SRC_HTTP_HTTPSERVER_H
