#include "src/http/HttpServer.h"

#include <functional>

#include <muduo/base/Logging.h>

#include "src/http/HttpContext.h"
#include "src/http/HttpRequest.h"
#include "src/http/HttpResponse.h"

using namespace webserver;
using namespace webserver::http;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;

using namespace webserver;
using namespace webserver::http;

namespace detail {

const std::string getFileType(HttpRequest *req) {
  /* 判断文件类型 */
  std::string path = req->getPath();
  std::string::size_type idx = path.find_last_of('.');
  if (idx == std::string::npos) {
    return "text/plain";
  }
  std::string suffix = path.substr(idx);
  if (HttpResponse::SUFFIX_TYPE.count(suffix) == 1) {
    return HttpResponse::SUFFIX_TYPE.find(suffix)->second;
  }
  return "text/plain";
}

void setResponseBody(const std::string &sourceDir, HttpRequest *req,
                     HttpResponse *resp) {}

void defaultHttpCallback(const HttpRequest &req, HttpResponse *resp) {
  /**
   * 1、根据请求路径确认需要加载的资源(文件)路径
   * 2、查找该文件是否存在或者是否为目录(是否404)
   * 3、判断是否有该文件的访问权限(是否为403)
   * 4、是否能open和mmap该文件(是否404，应该是500？)
   * 5、获取文件内容，设置Content-Length、Content-Type
   * 6、根据上述情况设置状态码
   * 7、设置HTTP响应报文的body
   */
  resp->setStatusCode(HttpResponse::k404NotFound);
  resp->setStatusMessage("Not Found");
  resp->setCloseConnection(true);
}

} // namespace detail

HttpServer::HttpServer(muduo::net::EventLoop *loop,
                       const muduo::net::InetAddress &listenAddr,
                       const std::string &name, const std::string &sourceDir,
                       muduo::net::TcpServer::Option option)
    : server_(loop, listenAddr, name, option), sourceDir_(sourceDir),
      httpCallback_(detail::defaultHttpCallback) {
  server_.setConnectionCallback(std::bind(&HttpServer::onConnection, this, _1));
  server_.setMessageCallback(
      std::bind(&HttpServer::onMessage, this, _1, _2, _3));
}

void HttpServer::start() {
  LOG_WARN << "HttpServer[" << server_.name() << "] starts listening on "
           << server_.ipPort();
  server_.start();
}

void HttpServer::onConnection(const muduo::net::TcpConnectionPtr &conn) {
  if (conn->connected()) {
    conn->setContext(HttpContext());
  }
}

void HttpServer::onMessage(const muduo::net::TcpConnectionPtr &conn,
                           muduo::net::Buffer *buf,
                           muduo::Timestamp receiveTime) {
  HttpContext *context =
      boost::any_cast<HttpContext>(conn->getMutableContext());

  if (!context->parseRequest(buf, receiveTime)) {
    conn->send("HTTP/1.1 400 Bad Request\r\n\r\n");
    conn->shutdown();
  }

  if (context->gotAll()) {
    onRequest(conn, context->request());
    context->reset();
  }
}

void HttpServer::onRequest(const muduo::net::TcpConnectionPtr &conn,
                           const HttpRequest &req) {
  const std::string &connection = req.getHeader("Connection");
  bool close =
      connection == "close" ||
      (req.getVersion() == HttpRequest::kHttp10 && connection != "Keep-Alive");
  HttpResponse response(close);
  httpCallback_(req, &response);
  muduo::net::Buffer buf;
  response.appendToBuffer(&buf);
  conn->send(&buf);
  if (response.closeConnection()) {
    conn->shutdown();
  }
}