#include "src/http/HttpRequest.h"

using namespace webserver;
using namespace webserver::http;

bool HttpRequest::setMethod(const std::string method) {
  assert(method_ == kInvalid);
  if (method == "GET") {
    method_ = kGet;
  } else if (method == "POST") {
    method_ = kPost;
  } else if (method == "HEAD") {
    method_ = kHead;
  } else if (method == "PUT") {
    method_ = kPut;
  } else if (method == "DELETE") {
    method_ = kDelete;
  } else {
    method_ = kInvalid;
  }
  return method_ != kInvalid;
}

const std::string HttpRequest::getMethod() const {
  const char *result = "UNKNOWN";
  switch (method_) {
  case kGet:
    result = "GET";
    break;
  case kPost:
    result = "POST";
    break;
  case kHead:
    result = "HEAD";
    break;
  case kPut:
    result = "PUT";
    break;
  case kDelete:
    result = "DELETE";
    break;
  default:
    break;
  }
  return result;
}

void HttpRequest::addHeader(const char *start, const char *colon,
                            const char *end) {
  std::string field(start, colon);
  ++colon;
  while (colon < end && isspace(*colon)) {
    ++colon;
  }
  std::string value(colon, end);
  // 去除value的右边的空格
  while (!value.empty() && isspace(value[value.size() - 1])) {
    value.resize(value.size() - 1);
  }
  headers_[field] = value;
}

std::string HttpRequest::getHeader(const std::string &field) const {
  std::string result;
  std::unordered_map<std::string, std::string>::const_iterator it =
      headers_.find(field);
  if (it != headers_.end()) {
    result = it->second;
  }
  return result;
}

const std::string HttpRequest::getFormData(const std::string &key) const {
  std::string result;
  std::unordered_map<std::string, std::string>::const_iterator it =
      formData_.find(key);
  if (it != formData_.end()) {
    result = it->second;
  }
  return result;
}

const std::string HttpRequest::getParameter(const std::string &key) const {
  std::string result;
  std::unordered_map<std::string, std::string>::const_iterator it =
      parameters_.find(key);
  if (it != parameters_.end()) {
    result = it->second;
  }
  return result;
}

const std::string HttpRequest::getUrlencodedData(const std::string &key) const {
  std::string result;
  std::unordered_map<std::string, std::string>::const_iterator it =
      xWwwFormUrlencodedData_.find(key);
  if (it != xWwwFormUrlencodedData_.end()) {
    result = it->second;
  }
  return result;
}

const std::string HttpRequest::getContentType() const {
  std::string result;
  std::unordered_map<std::string, std::string>::const_iterator it =
      headers_.find("Content-Type");
  if (it != headers_.end()) {
    result = it->second;
  }
  return result;
}

void HttpRequest::swap(HttpRequest &that) {
  std::swap(method_, that.method_);
  std::swap(version_, that.version_);
  path_.swap(that.path_);
  receiveTime_.swap(that.receiveTime_);
  headers_.swap(that.headers_);
  parameters_.swap(that.parameters_);
}