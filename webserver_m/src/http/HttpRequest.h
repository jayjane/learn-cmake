#ifndef SRC_HTTP_HTTPREQUEST_H
#define SRC_HTTP_HTTPREQUEST_H

#include <assert.h>
#include <stdio.h>

#include <unordered_map>

#include <muduo/base/Timestamp.h>
#include <muduo/base/Types.h>
#include <muduo/base/copyable.h>

#include "src/util/json/JsonParser.h"

namespace webserver {
namespace http {

class HttpRequest : public muduo::copyable {
public:
  using JsonData = util::json::wJson;
  enum Method { kInvalid, kGet, kPost, kHead, kPut, kDelete };
  enum Version { kUnknown, kHttp10, kHttp11 };

public:
  HttpRequest() : method_(kInvalid), version_(kUnknown) {}

  // getter and setter of HTTP request method (method_)
  bool setMethod(const char *start, const char *end) {
    return setMethod(std::string(start, end));
  }

  bool setMethod(const std::string method);

  Method method() const { return method_; }

  const std::string getMethod() const;

  const char *methodString() const { return getMethod().c_str(); }

  // getter and setter of HTTP version (version_)
  void setVersion(Version v) { version_ = v; }

  Version getVersion() const { return version_; }

  // getter and setter of HTTP request path (path_)
  void setPath(const char *start, const char *end) { path_.assign(start, end); }

  void setPath(const std::string &path) { path_ = path; }

  const std::string &getPath() const { return path_; }

  // getter and setter of HTTP request body (body_)
  void setBody(const std::string &body) { body_ = body; }

  const std::string &getBody() const { return body_; }

  // getter and setter of HTTP request packets receive time (receiveTime_)
  void setReceiveTime(muduo::Timestamp t) { receiveTime_ = t; }

  muduo::Timestamp getReceiveTime() const { return receiveTime_; }

  // getter and setter of HTTP request packet's header (headers_)
  void addHeader(const char *start, const char *colon, const char *end);

  void addHeader(const std::string &key, const std::string &value) {
    headers_[key] = value;
  }

  std::string getHeader(const std::string &field) const;

  const std::unordered_map<std::string, std::string> &getAllHeaders() const {
    return headers_;
  }

  // getter and setter of parameters after HTTP request path (parameters_)
  void addFormData(const std::string &key, const std::string &value) {
    parameters_[key] = value;
  }

  const std::string getParameter(const std::string &key) const;

  const std::unordered_map<std::string, std::string> &getAllFormData() const {
    return parameters_;
  }

  // getter and setter of parameters after HTTP request body (in form-data
  // style)
  void addParameter(const std::string &key, const std::string &value) {
    parameters_[key] = value;
  }

  const std::string getFormData(const std::string &key) const;

  const std::unordered_map<std::string, std::string> &getAllParameters() const {
    return formData_;
  }

  // getter and setter of parameters after HTTP request body
  // (x-www-form-urlencoded)
  void addUrlencodedData(const std::string &key, const std::string &value) {
    xWwwFormUrlencodedData_[key] = value;
  }

  const std::string getUrlencodedData(const std::string &key) const;

  const std::unordered_map<std::string, std::string> &
  getAllUrlencodedData() const {
    return xWwwFormUrlencodedData_;
  }

  // setter and getter of JsonData
  void setJsonData(const JsonData &jsonData) { jsonData_ = jsonData; }

  JsonData getJsonData() const { return jsonData_; }

  const std::string getContentType() const;

  // swap content
  void swap(HttpRequest &that);

private:
  Method method_;
  Version version_;
  std::string path_;
  std::string body_;
  muduo::Timestamp receiveTime_;
  std::unordered_map<std::string, std::string> headers_;
  std::unordered_map<std::string, std::string> parameters_;
  std::unordered_map<std::string, std::string> formData_;
  std::unordered_map<std::string, std::string> xWwwFormUrlencodedData_;
  JsonData jsonData_;
};

} // namespace http
} // namespace webserver

/**
 * 对于HTTP请求报文，需要解析的Content-Type的类型无非这几种
 * 左边是Postman的值，右边是Content-Type的值
 * Body-raw-Text: text/plain
 * Body-raw-JavaScript: application/javascript
 *
 * Body-raw-Json: application/json
 * {
    "name": "hd",
    "sex" : "man"
   }

 * Body-raw-HTML: text/html
 *
 * Body-raw-XML: application/xml
 *
 * Body-form-data: multipart/form-data(还有boundary作为分界，数据的前后都有,
 下面每一行都有\r\n)
 * Content-Type: multipart/form-data;boundary=--------699148017742590488981564
 *      ----------------------------699148017742590488981564
        Content-Disposition: form-data; name="name"

        hd
        ----------------------------699148017742590488981564
        Content-Disposition: form-data; name="sex"

        man
        ----------------------------699148017742590488981564--

 * Body-x-www-form-urlencoded: application/x-www-form-urlencoded
 *      name=hd&sex=man
 * */
#endif // SRC_HTTP_HTTPREQUEST_H
