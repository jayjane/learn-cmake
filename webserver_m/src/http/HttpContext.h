#ifndef SRC_HTTP_HTTPCONTEXTT_H
#define SRC_HTTP_HTTPCONTEXTT_H

#include <muduo/base/copyable.h>
#include <muduo/net/Buffer.h>

#include "src/http/HttpRequest.h"

namespace webserver {
namespace http {

class HttpContext : public muduo::copyable {
public:
  enum HttpRequestParseState {
    kExpectRequestLine,
    kExpectHeaders,
    kExpectBody,
    kGotAll,
  };

  HttpContext() : state_(kExpectRequestLine) {}

  // default copy-ctor, dtor and assignment are fine

  // return false if any error
  bool parseRequest(muduo::net::Buffer *buf, muduo::Timestamp receiveTime);

  bool gotAll() const { return state_ == kGotAll; }

  void reset() {
    state_ = kExpectRequestLine;
    HttpRequest dummy;
    request_.swap(dummy);
  }

  const HttpRequest &request() const { return request_; }

  HttpRequest &request() { return request_; }

private:
  bool processRequestLine(const std::string &requestLine);

  HttpRequestParseState state_;
  HttpRequest request_;
};

} // namespace http
} // namespace webserver

#endif // SRC_HTTP_HTTPCONTEXTT_H
