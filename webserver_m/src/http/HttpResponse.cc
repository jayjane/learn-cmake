#include "src/http/HttpResponse.h"

using namespace webserver;
using namespace webserver::http;

const std::unordered_map<std::string, std::string> HttpResponse::SUFFIX_TYPE = {
    {".html", "text/html"},
    {".xml", "text/xml"},
    {".xhtml", "application/xhtml+xml"},
    {".txt", "text/plain"},
    {".rtf", "application/rtf"},
    {".pdf", "application/pdf"},
    {".word", "application/nsword"},
    {".png", "image/png"},
    {".gif", "image/gif"},
    {".jpg", "image/jpeg"},
    {".jpeg", "image/jpeg"},
    {".au", "audio/basic"},
    {".mpeg", "video/mpeg"},
    {".mpg", "video/mpeg"},
    {".avi", "video/x-msvideo"},
    {".gz", "application/x-gzip"},
    {".tar", "application/x-tar"},
    {".css", "text/css "},
    {".js", "text/javascript "},
    {".mp4", "video/mp4"},
};

const std::unordered_map<HttpResponse::HttpStatusCode, std::string>
    HttpResponse::CODE_STATUS = {
        {k200Ok, "OK"},
        {k301MovedPermanently, "Moved Permanently"},
        {k400BadRequest, "Bad Request"},
        {k403Forbidden, "Forbidden"},
        {k404NotFound, "Not Found"},
};

const std::unordered_map<HttpResponse::HttpStatusCode, std::string>
    HttpResponse::CODE_PATH = {
        {k400BadRequest, "/400.html"},
        {k403Forbidden, "/403.html"},
        {k404NotFound, "/404.html"},
};

void HttpResponse::errorResponse(HttpStatusCode httpStatusCode,
                                 const std::string &errMessage) {
  setStatusCode(httpStatusCode);
  setStatusMessage(CODE_STATUS.find(httpStatusCode)->second);
  setContentType("text/plain");
  std::string msg = std::to_string(httpStatusCode) + " " +
                    CODE_STATUS.find(httpStatusCode)->second + "\r\n" +
                    errMessage;
  addHeader("Content-Length", std::to_string(msg.size()));
  setBody(msg);
}

void HttpResponse::appendToBuffer(muduo::net::Buffer *output) const {
  char buf[32];
  snprintf(buf, sizeof buf, "HTTP/1.1 %d ", statusCode_);
  output->append(buf);
  output->append(statusMessage_);
  output->append("\r\n");

  if (closeConnection_) {
    output->append("Connection: close\r\n");
  } else {
    snprintf(buf, sizeof buf, "Content-Length: %zd\r\n", body_.size());
    output->append(buf);
    output->append("Connection: Keep-Alive\r\n");
  }

  for (const auto &header : headers_) {
    output->append(header.first);
    output->append(": ");
    output->append(header.second);
    output->append("\r\n");
  }

  output->append("\r\n");
  output->append(body_);

  //   std::cout << output->retrieveAllAsString() << std::endl;
}