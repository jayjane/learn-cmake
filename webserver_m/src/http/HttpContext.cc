#include "src/http/HttpContext.h"

#include <regex>

#include <muduo/base/Logging.h>

using namespace webserver;
using namespace webserver::http;

namespace detail {

bool parsePathAndParameters(HttpRequest *req, const std::string &path) {
  // path的格式可能是/aa/bb，或者是/aa/bb?key1=value1&key2=value2&....
  bool succeed = false;
  std::regex reg("([^\?]*)\\?([^\?]*)");
  std::smatch match;
  if (std::regex_match(path, match, reg)) { // 如果请求路径包含问号?
    req->setPath(match[1]); // 问号前面的部分内容为请求路径
    // ?后面的为参数（格式为 key1=value1&key2=value2&....）
    const std::string params = match[2];
    // 下面解析params
    size_t pos_l = 0, pos_r = 0;
    reg = std::regex("([^&]*)=([^&]*)");
    std::string param;
    while (pos_r != params.size()) {
      pos_r = params.find('&', pos_l);
      if (pos_r == std::string::npos) {
        pos_r = params.size();
        param = std::string(&params[pos_l], &params[pos_r - 1] + 1);
      } else {
        param = std::string(&params[pos_l], &params[pos_r]);
      }
      if (regex_match(param, match, reg)) {
        req->addParameter(match[1], match[2]);
      } else {
        succeed = false;
        break;
      }
      pos_l = pos_r + 1;
    }
    succeed = true;
  } else {
    req->setPath(path);
    succeed = true;
  }
  return succeed;
}

bool parseVersion(HttpRequest *req, const std::string &version) {
  std::regex reg = std::regex("HTTP/1.[01]");
  std::smatch match;
  if (std::regex_match(version, match, reg)) {
    if (version[version.size() - 1] == '0') {
      req->setVersion(HttpRequest::kHttp10);
    } else {
      req->setVersion(HttpRequest::kHttp11);
    }
    return true;
  } else {
    return false;
  }
}

bool parseText(HttpRequest *req, const std::string &body) {
  // Todo
  return false;
}

bool parseJavaScript(HttpRequest *req, const std::string &body) {
  // Todo
  return false;
}

bool parseJson(HttpRequest *req, const std::string &body) {
  webserver::util::json::wJson res =
      webserver::util::json::JsonParser::parse(body);
  if (res.isNull()) {
    LOG_INFO << "JSON对象为空";
    return false;
  } else {
    req->setJsonData(res);
    return true;
  }
}

bool parseHTML(HttpRequest *req, const std::string &body) {
  // Todo
  return false;
}

bool parseXML(HttpRequest *req, const std::string &body) {
  // Todo
  return false;
}

bool parseFormData(HttpRequest *req, const std::string &body) {
  LOG_INFO << body[body.size() - 1];
  LOG_INFO << body[body.size() - 2];
  LOG_INFO << body[body.size() - 3];
  const std::string contentType = req->getHeader("Content-Type");
  if (contentType.size() == 0) {
    return false;
  }
  LOG_INFO << contentType;
  std::regex reg("multipart/form-data; boundary=-+([0-9]+)");
  std::smatch match;
  if (std::regex_match(contentType, match, reg)) {
    std::string boundary = "^-+" + std::string(match[1]);
    LOG_INFO << "boundary=" << boundary;
    std::regex reg1(boundary);
    std::regex reg2(
        "Content-Disposition: form-data; name=\"([(0-9)(a-z)(A-Z)(_)]*)\"");
    const char *kCRLF = "\r\n";
    const char *bodyEnd = &body[body.size() - 1];
    const char *pos1 = body.data();
    const char *pos2 = std::search(pos1, bodyEnd, kCRLF, kCRLF + 2);
    std::string key, value;
    std::string line = std::string(pos1, pos2);
    if (!std::regex_match(line, reg1)) {
      return false;
    }

    while (pos2 != bodyEnd) {
      pos1 = pos2 + 2;
      pos2 = std::search(pos1, bodyEnd, kCRLF, kCRLF + 2);
      line = std::string(pos1, pos2);
      LOG_INFO << line;
      if (std::regex_match(line, match, reg2)) {
        key = match[1];
        LOG_INFO << "key=" << key;
      } else {
        LOG_INFO << "匹配key失败";
        return false;
      }

      pos1 = pos2 + 2;
      pos2 = std::search(pos1, bodyEnd, kCRLF, kCRLF + 2);
      if (pos1 != pos2) {
        LOG_INFO << "空行失败";
        return false;
      }

      pos1 = pos2 + 2;
      pos2 = std::search(pos1, bodyEnd, kCRLF, kCRLF + 2);
      value = std::string(pos1, pos2);
      LOG_INFO << "value=" << value;
      req->addFormData(key, value);

      pos1 = pos2 + 2;
      pos2 = std::search(pos1, bodyEnd, kCRLF, kCRLF + 2);
      line = std::string(pos1, pos2);
      LOG_INFO << "line.size()=" << line.size() << "," << line[line.size() - 1]
               << ",\n";
      if (line[line.size() - 1] == '\r') {
        LOG_ERROR << "cao";
      }

      LOG_INFO << line;
      if (!std::regex_match(line, reg1)) {
        LOG_INFO << "boundary匹配失败";
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
  /*
  boundary=--------------------------769563161532749443670648
  ----------------------------769563161532749443670648   52
  Content-Disposition: form-data; name="name"            43

  hd                                                     2
  ----------------------------769563161532749443670648   52
  Content-Disposition: form-data; name="sex"             42

  man                                                    3
  ----------------------------769563161532749443670648-- 54
  */
}

bool parse_x_www_form_urlencoded(HttpRequest *req, const std::string &body) {
  const char separator = '&';
  std::regex reg("([^&=]*)=([^&=]*)");
  std::smatch match;
  std::size_t pos1 = 0;
  std::size_t pos2 = body.find(separator, pos1);
  std::string kvPair;
  while (true) {
    kvPair = body.substr(pos1, pos2 - pos1);
    if (std::regex_match(kvPair, match, reg)) {
      req->addUrlencodedData(match[1], match[2]);
    } else {
      return false;
    }

    pos1 = pos2 + 1;
    pos2 = body.find(separator, pos1);
    if (pos2 == std::string::npos) {
      pos2 = body.size();
      kvPair = body.substr(pos1, pos2 - pos1);
      if (std::regex_match(kvPair, match, reg)) {
        req->addUrlencodedData(match[1], match[2]);
      } else {
        return false;
      }
      break;
    }
  }
  return true;
}

} // namespace detail

// return false if any error
bool HttpContext::processRequestLine(const std::string &requestLine) {
  std::regex reg("([^ ]*) ([^ ]*) ([^ ]*)");
  std::smatch match;
  if (std::regex_match(requestLine, match, reg)) {
    // 依次解析请求方法，请求路径、协议版本
    return request_.setMethod(match[1]) &&
           detail::parsePathAndParameters(&request_, match[2]) &&
           detail::parseVersion(&request_, match[3]);
  } else {
    return false;
  }
}

// return false if any error
bool HttpContext::parseRequest(muduo::net::Buffer *buf,
                               muduo::Timestamp receiveTime) {
  bool ok = true;
  bool hasMore = true;
  unsigned long long bodyLength = 0;
  while (hasMore) {
    if (state_ == kExpectRequestLine) {
      const char *crlf = buf->findCRLF();
      if (crlf) {
        const std::string requestLine(buf->peek(), crlf);
        ok = processRequestLine(requestLine);
        if (ok) {
          request_.setReceiveTime(receiveTime);
          buf->retrieveUntil(crlf + 2);
          state_ = kExpectHeaders;
        } else {
          hasMore = false;
        }
      } else {
        hasMore = false;
      }
    } else if (state_ == kExpectHeaders) {
      const char *crlf = buf->findCRLF();
      if (crlf) {
        const char *colon = std::find(buf->peek(), crlf, ':');
        if (colon != crlf) {
          request_.addHeader(buf->peek(), colon, crlf);
        } else {
          // empty line, end of header
          // 此时最近的一个/r/n前面已经没有冒号了，有两种情况：
          // 1、没有请求体
          // 2、有请求体

          // 接下来根据头部字段Content-Length来判断是第1种还是第2种情况
          //   Todo: Content-Length也有可能不存在
          std::string lenStr = request_.getHeader("Content-Length");
          if (lenStr.size() == 0 || std::stoull(lenStr) == 0) {
            state_ = kGotAll;
            hasMore = false;
          } else {
            bodyLength = std::stoull(lenStr);
            state_ = kExpectBody;
          }
        }
        buf->retrieveUntil(crlf + 2);
      } else {
        hasMore = false;
      }
    } else if (state_ == kExpectBody) {
      const std::string body(buf->peek(), bodyLength);
      buf->retrieve(static_cast<size_t>(bodyLength));
      request_.setBody(body);

      std::string contentType = request_.getHeader("Content-Type");
      if (contentType == "application/x-www-form-urlencoded") {
        if (!detail::parse_x_www_form_urlencoded(&request_, body)) {
          return false;
        }
      } else if (contentType == "application/json") {
        if (!detail::parseJson(&request_, body)) {
          return false;
        }
      } else {
        // Todo: 支持其他Content-Type 的 请求体解析
        // multipart/form-data
        return false;
      }

      state_ = kGotAll;
      hasMore = false;
    }
  }
  return ok;
}
