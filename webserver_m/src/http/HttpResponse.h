#ifndef SRC_HTTP_HTTPRESPONSE_H
#define SRC_HTTP_HTTPRESPONSE_H

#include <unordered_map>

#include <muduo/base/Types.h>
#include <muduo/base/copyable.h>
#include <muduo/net/Buffer.h>

namespace webserver {
namespace http {

class Buffer;
class HttpResponse : public muduo::copyable {
public:
  enum HttpStatusCode {
    kUnknown,
    k200Ok = 200,
    k301MovedPermanently = 301,
    k400BadRequest = 400,
    k403Forbidden = 403,
    k404NotFound = 404,
  };

  static const std::unordered_map<std::string, std::string> SUFFIX_TYPE;
  static const std::unordered_map<HttpStatusCode, std::string> CODE_STATUS;
  static const std::unordered_map<HttpStatusCode, std::string> CODE_PATH;

  explicit HttpResponse(bool close)
      : statusCode_(kUnknown), closeConnection_(close) {}

  void setStatusCode(HttpStatusCode code) { statusCode_ = code; }

  HttpStatusCode getStatusCode() const { return statusCode_; }

  void setStatusMessage(const std::string &message) {
    statusMessage_ = message;
  }

  void setCloseConnection(bool on) { closeConnection_ = on; }

  bool closeConnection() const { return closeConnection_; }

  void setContentType(const std::string &contentType) {
    addHeader("Content-Type", contentType);
  }

  // FIXME: replace std::string with std::stringPiece
  void addHeader(const std::string &key, const std::string &value) {
    headers_[key] = value;
  }

  void setBody(const std::string &body) { body_ = body; }

  void errorResponse(HttpStatusCode httpStatusCode,
                     const std::string &errMessage);

  void appendToBuffer(muduo::net::Buffer *output) const;

private:
  std::unordered_map<std::string, std::string> headers_;
  HttpStatusCode statusCode_;
  // FIXME: add http version
  std::string statusMessage_;
  bool closeConnection_;
  std::string body_;
};

} // namespace http
} // namespace webserver

#endif // SRC_HTTP_HTTPRESPONSE_H
