# MySQL数据库连接池
> 在高并发的情况，大量的TCP三次握手，MySQL server连接认证，MySQL server连接关闭回收资源，TCP四次挥手会耗费性能。本项目的目的是为了避免频繁的向数据库申请资源，释放资源带来性能损耗。

> 为数据库的连接建立一个缓存池，预先在该缓存中放入一定数量的连接。当多个任务需要访问mysql时，不需要每个任务都去直接通过TCP连接mysql server，而是在该缓存池中取对应数量的连接即可。用完之后不需要释放该连接，只需要归还到连接池即可。

## 数据库连接封装（Connection.{h & cc}）
### 主要的三个接口:
* connect：通过传入关键信息来连接数据库，具体封装`mysql_real_connect`。
* update：执行数据库更新操作，其返回值为bool类型，表示更新操作成功与否。
* query：执行数据库查询操作，返回值为`MYSQL_RES`类型（Todo：封装MYSQL_RES）。
* query和update操作其实都是封装`mysql_query`，只是由于两者造成的效果以及需要返回的结果不同，所以此处分为两个接口。
### 剩下的两个接口：
* refreshAliveTime：每个数据库连接都会记录连接上次工作的时间，每当该通过该连接执行操作后，都需要调用refreshAliveTime来刷新这个时间（连接池来通过这个时间将一些空闲连接释放掉）。
* getAliveTime：返回连接持续时间（now - 连接建立时间）。
* （数据库连接池ConnectionPool）通过上面两个接口来判断该连接是否需要释放。

## 连接池的封装（ConnectionPool.{h & cc}）
### 明确的点：
* 池中放的是Connection（数据库连接）。
### 需要确定的点：
* 池中连接数是否固定；若不固定，如何规定连接数量。
* 创建连接的时机。
* 释放连接的时机。
### 此处的方案：
* 连接池是通过**懒汉单例模式**实现。
* 连接池的配置通过加载配置文件获取。
* 连接池中的连接数量不是固定的，而是在`[minSize_, maxSize_]`这个范围内。**注意**：maxSize_指的是池中连接数量与被取走的连接数量总和的最大值。
* 通过两个线程来管理连接，一个是`produce`，负责连接的创建；一个是`scanner`，负责定时扫描并释放掉池中空闲连接。这两个线程自创建后一直运行（创建后就detach）
具体实现：
首先在构造函数时：

1、加载配置文件，配置信息除了数据库的信息外，还有maxIdleTime_（最大空闲时间）、connectionTimeout_（超时时间）。`maxIdleTime_`的作用：当一个连接距离上次使用已经超过maxIdleTime_，可以认为这个连接是空闲连接，可以释放掉。`connectionTimeout_`：当从连接池中去获取一个连接时，如果连接池中为空，每等待connectionTimeout_就检查一次队列是否为空或者有新连接加入池中。connectionTimeout_的另一层含义：数据库连接建立要么在connectionTimeout_这段时间内成功建立，要么就超时，建立失败，所以需要再次检查池中是否为空。

2、从池中取线程，如果池中有连接，直接将返回一个连接，并通过条件变量通知(唤醒)`produce`线程；如果池中没有连接，就需要等待连接的创建，每connectionTimeout_检查一次池中是否依然为空或者此间有新连接创建.
注意：池中存的线程是直接存放Connection，而取出是以shared_ptr封装的，还应注意到，此处为shared_ptr指定的deleter，为的是让shared_ptr生命周期结束后，并不是真正的释放连接，而是将其放回池中。这里用到lambda表达式的引用捕获，而且[&]代表捕获当前作用域下的所有局部变量的引用，这样才可以通过connectionQueue_将连接push回去。
```c++
// 对于使用完成的连接，不能直接销毁该连接，而是需要将该连接归还给连接池的队列，
// 供之后的其他消费者使用，于是我们使用智能指针，自定义其析构函数，完成放回的操作：
std::shared_ptr<Connection> connection(connectionQueue_.front(),
                                [&](Connection *conn) {
                                    std::unique_lock<std::mutex> locker(mtx_);
                                    conn->refreshAliveTime();
                                    connectionQueue_.push(conn);
                                });
```

3、创建上面提到的两个线程：
3.1、`produce`：当池中连接数大于或等于minSize_，就需要阻塞等待，直到有其他线程从池中取走线程，然后检查池中已创建的连接数量，若小于maxSize_。就往池中放一个连接，并唤醒由于取连接而阻塞等的线程。一直循环。
3.2、`scanner`：每过（sleep）maxIdleTime_，就检查池中的空闲连接，并将其释放，直至池中连接小于等于minSize_。

线程池性能测试简单测试：(不严谨的)
测试程序见：test/src/util/sql/ConnectionPool_bench.cc
测试结果为：（此处线程池的最小连接数为8，最大连接数为16）
![image1000](./pictures/1000.png)
![image10000](./pictures/10000.png)
![image100000](./pictures/100000.png)