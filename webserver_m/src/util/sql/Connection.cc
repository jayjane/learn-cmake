#include "src/util/sql/Connection.h"

using namespace webserver::util::sql;

Connection::Connection() {
  conn_ = mysql_init(nullptr);
  mysql_set_character_set(conn_, "utf8"); // 设置编码格式维utf8
}

Connection::~Connection() {
  if (conn_ != nullptr) {
    mysql_close(conn_);
  }
}

bool Connection::connect(const std::string &ip, const uint16_t port,
                         const std::string &user, const std::string &pwd,
                         const std::string &db) {
  conn_ = mysql_real_connect(conn_, ip.c_str(), user.c_str(), pwd.c_str(),
                             db.c_str(), port, nullptr, 0);
  if (conn_ == nullptr) {
    LOG_ERROR << "MySQL Connect Error";
    return false;
  }
  return true;
}

bool Connection::update(const std::string &sql) {
  if (mysql_query(conn_, sql.c_str()) != 0) {
    LOG_INFO << "SQL:(" << sql << ") failed to update, " << mysql_error(conn_);
    return false;
  }
  return true;
}

MYSQL_RES *Connection::query(const std::string &sql) {
  if (mysql_query(conn_, sql.c_str()) != 0) {
    LOG_INFO << "SQL:(" << sql << ") failed to query, " << mysql_error(conn_);
    return nullptr;
  }
  return mysql_use_result(conn_);
}

void Connection::refreshAliveTime() {
  aliveTime_ = std::chrono::steady_clock::now();
}

long long Connection::getAliveTime() const {
  return std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::steady_clock::now() - aliveTime_)
      .count();
}
