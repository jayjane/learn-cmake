#ifndef SRC_UTIL_SQL_CONNECTIONPOOL_H
#define SRC_UTIL_SQL_CONNECTIONPOOL_H

#include "src/util/sql/Connection.h"

#include <muduo/base/noncopyable.h>

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

namespace webserver {
namespace util {
namespace sql {

class ConnectionPool : public muduo::noncopyable {
 public:
  static ConnectionPool &getConnectionPool();  // 获取连接池对象实例

  /**
   * @brief 从连接池中获取一个可用的空闲连接
   *
   * @return 智能指针管理的MySQL连接
   */
  std::shared_ptr<Connection> getConnection();

  ~ConnectionPool();

 private:
  ConnectionPool();

  /**
   * @brief 从json文件加载数据库配置
   *
   * @param jsonFilePath json配置文件的路径
   * @return true 配置正确
   */
  bool loadConfigFile(const std::string &jsonFilePath);

  // 运行在独立的线程中，专门负责生产新连接
  void produceConnectionTask();

  // 扫描超过maxIdleTime时间的空闲连接，进行对于的连接回收
  void scannerConnectionTask();

  void addConnection();

 public:  // some getter
  const std::string &getIP() { return ip_; }

  uint16_t getPort() { return port_; }

  const std::string getUserName() { return user_; }

  const std::string &getUserPassword() { return pwd_; }

  const std::string &getDataBaseName() { return db_; }

  uint32_t getMinSize() { return minSize_; }

  uint32_t getMaxSize() { return maxSize_; }

  uint32_t getMaxIdleTime() { return maxIdleTime_; }

  uint32_t getConnectionTimeout() { return connectionTimeout_; }

  uint32_t getCurConnCnt() { return connectionCount_; }

 private:
  std::string ip_;    // MySQL数据库所在主机的IP
  uint16_t port_;     // MySQL数据库所在主机的端口
  std::string user_;  // MySQL用户名
  std::string pwd_;   // MySQL用户密码
  std::string db_;    // MySQL数据库名字

  uint32_t minSize_;            // 初始链接数量
  uint32_t maxSize_;            // 最大连接数量
  uint32_t maxIdleTime_;        // 最大空闲时间
  uint32_t connectionTimeout_;  // 超时时间

  std::queue<Connection *> connectionQueue_;  // 存储连接队列
  std::mutex mtx_;  // 维护连接队列的线程安全互斥锁
  std::atomic_uint32_t
      connectionCount_;  // 记录连接所创建的connection连接的总数量
  std::condition_variable
      cond_;  // 设置条件变量，用于连接生产线程和连接消费线程的通信
};

}  // namespace sql
}  // namespace util
}  // namespace webserver

#endif  // !SRC_UTIL_SQL_CONNECTIONPOOL_H
