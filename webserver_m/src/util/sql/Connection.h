#ifndef SRC_UTIL_SQL_CONNECTION_H
#define SRC_UTIL_SQL_CONNECTION_H

#include <muduo/base/Logging.h>

#include <chrono>
#include <mysql/mysql.h>
#include <string>

namespace webserver {
namespace util {
namespace sql {

class Connection {
public:
  Connection();

  ~Connection();

  /**
   * @brief 连接数据库
   * 
   * @param ip 数据库ip
   * @param port 数据库端口
   * @param user 数据库用户名
   * @param pwd 数据库密码
   * @param db 数据库schema
   * @return true 成功连接数据库
   * @return false 未能连接数据库
   */
  bool connect(const std::string &ip, const uint16_t port,
               const std::string &user, const std::string &pwd,
               const std::string &db);

  /// @brief 执行更新语句（UPDATE）
  /// @param sql SQL语句
  /// @return SQL执行是否成功
  bool update(const std::string &sql);

  /// @brief 执行查询语句（SELECT）
  /// @param sql SQL语句
  /// @return 查询结果集
  MYSQL_RES *query(const std::string &sql);

  /**
   * @brief 刷新MySQL连接开始工作的时间
   * 
   */
  void refreshAliveTime();

  /**
   * @brief 返回MySQL连接存活时间
   * 
   * @return long long MySQL连接存活时间，单位：微妙
   */
  long long getAliveTime() const;

private:
  // 底层的连接
  MYSQL *conn_;
  // 记录MySQL连接开始工作的时间
  std::chrono::time_point<std::chrono::steady_clock> aliveTime_;
};

} // namespace sql
} // namespace util
} // namespace webserver

#endif // !SRC_UTIL_SQL_CONNECTION_H
