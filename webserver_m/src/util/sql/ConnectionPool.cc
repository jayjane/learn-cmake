#include "src/util/sql/ConnectionPool.h"

#include "src/util/json/JsonParser.h"

using namespace webserver::util::sql;
using namespace webserver::util::json;

namespace webserver {
namespace util {
namespace sql {
namespace detail {

// if sth wrong, LOG_FATAL will terminate the program
/**
 * @brief 检查数据库连接池配置是否有错误
 *
 * @param root 存有数据库连接池配置的json对象
 */
void checkJson(wJson &root) {
  if (root.isNull()) {
    LOG_FATAL << "json configure file is empty or wrong syntax";
  }

  if (root["ip"].isNull() || !root["ip"].isString()) {
    LOG_FATAL << "no [ip] option in configure file or wrong syntax";
  }

  if (root["port"].isNull() || !root["port"].isUInt()) {
    LOG_FATAL << "no [port] option in configure file or wrong syntax";
  }

  if (root["user"].isNull() || !root["user"].isString()) {
    LOG_FATAL << "no [user] option in configure file or wrong syntax";
  }

  if (root["pwd"].isNull() || !root["pwd"].isString()) {
    LOG_FATAL << "no [pwd] option in configure file or wrong syntax";
  }

  if (root["db"].isNull() || !root["db"].isString()) {
    LOG_FATAL << "no [db] option in configure file or wrong syntax";
  }

  if (root["minSize"].isNull() || !root["minSize"].isUInt()) {
    LOG_FATAL << "no [minSize] option in configure file or wrong syntax";
  }

  if (root["maxSize"].isNull() || !root["maxSize"].isUInt()) {
    LOG_FATAL << "no [maxSize] option in configure file or wrong syntax";
  }

  if (root["maxIdleTime"].isNull() || !root["maxIdleTime"].isUInt()) {
    LOG_FATAL << "no [maxIdleTime] option in configure file or wrong syntax";
  }

  if (root["timeout"].isNull() || !root["timeout"].isUInt()) {
    LOG_FATAL << "no [timeout] option in configure file or wrong syntax";
  }
}

}  // namespace detail
}  // namespace sql
}  // namespace util
}  // namespace webserver

/**
 * @brief 懒汉单例模式获取数据库连接池对象
 *
 * @return ConnectionPool& 数据库连接池对象
 */
ConnectionPool &ConnectionPool::getConnectionPool() {
  static ConnectionPool pool;
  return pool;
}

// 相当于连接的消费者线程
std::shared_ptr<Connection> ConnectionPool::getConnection() {
  std::unique_lock<std::mutex> lock(mtx_);
  // 连接池为空，就阻塞等待connectionTimeout_时间
  // 如果时间过了，还没唤醒
  while (connectionQueue_.empty()) {
    if (std::cv_status::timeout ==
        cond_.wait_for(lock, std::chrono::microseconds(connectionTimeout_))) {
      if (connectionQueue_.empty()) {  // 就可能还是为空
        continue;
      }
    }
  }
  // 对于使用完成的连接，不能直接销毁该连接，而是需要将该连接归还给连接池的队列，
  // 供之后的其他消费者使用，于是我们使用智能指针，自定义其析构函数，完成放回的操作：
  std::shared_ptr<Connection> connection(connectionQueue_.front(),
                                   [&](Connection *conn) {
                                     std::unique_lock<std::mutex> locker(mtx_);
                                     conn->refreshAliveTime();
                                     connectionQueue_.push(conn);
                                   });
  connectionQueue_.pop();
  cond_.notify_all(); // 从线程池取出一个连接时，唤醒连接生产线程
  return connection;
}

ConnectionPool::ConnectionPool() {
  if (!loadConfigFile("/root/cmake-demo/webserver_m/test/src/util/sql/dbconf.json")) {
    LOG_ERROR << "JSON Config Error";
    return;
  }

  // 创建初始数量的连接
  for (size_t i = 0; i < minSize_; ++i) {
    addConnection();
  }

  // 启动一个新的线程，作为连接的生产者
  std::thread produce(std::bind(&ConnectionPool::produceConnectionTask, this));
  produce.detach();  // 守护线程，主线程结束了，这个线程就结束了

  // 启动一个新的定时线程，扫描超过maxIdleTime时间的空闲连接，进行对于的连接回收
  std::thread scanner(std::bind(&ConnectionPool::scannerConnectionTask, this));
  scanner.detach();
}

/**
 * @brief 将连接池中的所有连接全部销毁
 * 
 */
ConnectionPool::~ConnectionPool() {
  while (!connectionQueue_.empty()) {
    Connection *ptr = connectionQueue_.front();
    connectionQueue_.pop();
    delete ptr;
  }
}

bool ConnectionPool::loadConfigFile(const std::string &jsonFilePath) {
  wJson root = JsonParser::parseFromFile(jsonFilePath);
  detail::checkJson(root);

  ip_ = root["ip"].asString();
  port_ = static_cast<uint16_t>(root["port"].asUInt());
  user_ = root["user"].asString();
  pwd_ = root["pwd"].asString();
  db_ = root["db"].asString();
  minSize_ = root["minSize"].asUInt();
  maxSize_ = root["maxSize"].asUInt();
  maxIdleTime_ = root["maxIdleTime"].asUInt();
  connectionTimeout_ = root["timeout"].asUInt();

  return true;
}

void ConnectionPool::produceConnectionTask() {
  while (true) {
    // 连接消费者线程每取出一个连接，生产者线程就往队列添加一个连接
    std::unique_lock<std::mutex> lock(mtx_);
    while (connectionQueue_.size() >= minSize_) {
      cond_.wait(lock);
    }
    if (connectionCount_ < maxSize_) {
      addConnection();
    }
    cond_.notify_all();
  }
}

void ConnectionPool::scannerConnectionTask() {
  while (true) {
    std::this_thread::sleep_for(
        std::chrono::seconds(maxIdleTime_));  // 每隔maxIdleTime_秒检查一次
    std::lock_guard<std::mutex> lock(mtx_);
    // 如果此时连接数小于或等于minSize_，就不进行释放连接的操作
    while (connectionCount_ > minSize_) {
      // 队头的时间没超过，那后面的时间就都没超过
      Connection *ptr = connectionQueue_.front();
      if (ptr->getAliveTime() >= maxIdleTime_ * 1000) {
        connectionQueue_.pop();
        --connectionCount_;
        delete ptr;  // 真正的释放连接
      } else {
        break;
      }
    }
  }
}

void ConnectionPool::addConnection() {
  Connection *conn = new Connection();
  conn->connect(ip_, port_, user_, pwd_, db_);
  conn->refreshAliveTime();
  connectionQueue_.push(conn);
  ++connectionCount_;
}
