#ifndef SRC_UTIL_JSON_JSONPARSER_H
#define SRC_UTIL_JSON_JSONPARSER_H

#include <jsoncpp/json/json.h>

namespace webserver {
namespace util {
namespace json {

using wJson = Json::Value;

class JsonParser {
public:
  JsonParser() = default;
  ~JsonParser() = default;

  static wJson parse(const char *jsonData);
  static wJson parse(const std::string &jsonData);

  static wJson parseFromFile(const std::string &filePath);
  static wJson parseFromFile(const char *filePath);
};

} // namespace json
} // namespace util
} // namespace webserver

#endif /* SRC_UTIL_JSON_JSONPARSER_H */