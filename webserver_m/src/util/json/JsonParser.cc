#include "src/util/json/JsonParser.h"

#include <fstream>

#include <muduo/base/Logging.h>

using namespace webserver;
using namespace webserver::util;
using namespace webserver::util::json;
using namespace std;

wJson JsonParser::parse(const char *jsonData) {
  const std::string jsonString(jsonData);
  return JsonParser::parse(jsonData);
}

wJson JsonParser::parse(const std::string &jsonData) {
  wJson val, nullValue;
  Json::Reader reader;
  if (!reader.parse(jsonData, val)) {
    return nullValue;
  } else {
    return val;
  }
}

wJson JsonParser::parseFromFile(const std::string &filePath) {
  wJson val, nullValue;
  Json::Reader reader;
  std::ifstream in(filePath, std::ios::binary);
  LOG_TRACE << "JsonParser::parseFromFile filePath: " << filePath;
  if (!in.is_open()) {
    LOG_FATAL << "failed to open file: " << filePath;
  }
  if (!reader.parse(in, val)) {
    LOG_FATAL << "failed to parse file: " << filePath;
  }
  in.close();

  return val;
}

wJson JsonParser::parseFromFile(const char *filePath) {
  const std::string jsonFilePath(filePath);
  return parseFromFile(jsonFilePath);
}