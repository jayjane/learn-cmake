#include "src/web/Webserver.h"

#include <fcntl.h>    // open
#include <sys/mman.h> // mmap, munmap
#include <sys/stat.h> // stat
#include <unistd.h>   // close

#include <unordered_map>

using namespace muduo;
using namespace muduo::net;
using namespace webserver;
using namespace webserver::http;

Webserver::Webserver(const Config &config)
    : loop_(new muduo::net::EventLoop()),
      httpServer_(new HttpServer(loop_.get(), InetAddress(config.port_),
                                 config.serveName_, config.resourcesPath_)) {
  httpServer_->setThreadNum(config.threadNum_);
  httpServer_->setHttpCallback(std::bind(&Webserver::onRequest, this, _1, _2));
  Logger::setLogLevel(config.logLevel_);
}

void webserver::Webserver::start() {
  httpServer_->start();
  loop_->loop();
}

void Webserver::post(const std::string path, Handler handler) {
  pathTOmethod ptm(path, "POST");
  handlers_.emplace(ptm, handler);
}

void Webserver::get(const std::string path, Handler handler) {
  pathTOmethod ptm(path, "GET");
  handlers_.emplace(ptm, handler);
}

void Webserver::onRequest(const http::HttpRequest &req,
                          http::HttpResponse *resp) {
  const std::string requestPath = req.getPath();
  const std::string requestMethod = req.getMethod();
  pathTOmethod ptm(requestPath, requestMethod);
  const std::map<pathTOmethod, Handler>::const_iterator it =
      handlers_.find(ptm);
  if (it != handlers_.end()) {
    it->second(req, resp);
  } else {
    defaultOnRequest(req, resp);
  }
}

void Webserver::defaultOnRequest(const http::HttpRequest &req,
                                 http::HttpResponse *resp) {

  LOG_INFO << req.methodString() << " " << req.getPath();

  const std::unordered_map<std::string, std::string> &headers =
      req.getAllHeaders();
  for (const auto &header : headers) {
    LOG_INFO << header.first << ": " << header.second;
  }

  const std::unordered_map<std::string, std::string> &parameters =
      req.getAllParameters();
  for (const auto &parameter : parameters) {
    LOG_INFO << parameter.first << ": " << parameter.second;
  }

  const std::unordered_map<std::string, std::string> &urlEncodedData =
      req.getAllUrlencodedData();
  for (const auto &parameter : urlEncodedData) {
    LOG_INFO << parameter.first << ": " << parameter.second;
  }

  LOG_INFO << req.getBody();

  //

  // 拼接路径
  std::string reqPath = req.getPath() == "/" ? "/index.html" : req.getPath();

  // Todo: 校验用户登录和用户注册
  if (req.getMethod() == "POST" &&
      req.getHeader("Content-Type") == "application/x-www-form-urlencoded") {
    reqPath = "/welcome.html";
  }

  std::string filePath = httpServer_->getSourceDir() + reqPath;
  LOG_INFO << "请求路径为:" << filePath;

  // 尝试打开文件，并设置状态码
  struct stat mmFileStat;
  if (::stat(filePath.data(), &mmFileStat) < 0 || S_ISDIR(mmFileStat.st_mode)) {
    resp->setStatusCode(HttpResponse::HttpStatusCode::k404NotFound);
  } else if (!(mmFileStat.st_mode & S_IROTH)) {
    resp->setStatusCode(HttpResponse::HttpStatusCode::k403Forbidden);
  } else {
    resp->setStatusCode(HttpResponse::HttpStatusCode::k200Ok);
  }

  // 设置状态码对应的文本信息
  HttpResponse::HttpStatusCode httpStatusCode = resp->getStatusCode();
  resp->setStatusMessage(
      HttpResponse::CODE_STATUS.find(httpStatusCode)->second);

  // 如果状态码为4XX，就将文件路径更改为对应HTML文件的路径
  if (resp->getStatusCode() != HttpResponse::HttpStatusCode::k200Ok) {
    LOG_TRACE << "状态码为4XX";
    filePath = httpServer_->getSourceDir() +
               HttpResponse::CODE_PATH.find(httpStatusCode)->second;
    ::stat(filePath.data(), &mmFileStat);
  }

  int srcFd = ::open(filePath.data(), O_RDONLY);
  if (srcFd < 0) {
    /**
     * open文件失败，将状态码重新设置为404，
     * 发送文本信息后return
     */
    LOG_ERROR << "failed to open " << filePath;
    resp->errorResponse(HttpResponse::HttpStatusCode::k404NotFound,
                        "failed to open " + filePath);
    return;
  }
  LOG_TRACE << "open " << filePath << " successfully!";

  void *mmRet =
      mmap(NULL, mmFileStat.st_size, PROT_READ, MAP_PRIVATE, srcFd, 0);
  ::close(srcFd);

  if (mmRet == MAP_FAILED) {
    /**
     * mmap文件失败，将状态码重新设置为404，
     * 发送文本信息后return
     */
    LOG_ERROR << "failed to mmap " << filePath;
    resp->errorResponse(HttpResponse::HttpStatusCode::k404NotFound,
                        "failed to mmap " + filePath);
  } else { // mmap成功
    // 根据文件后缀名判断文件类型, 并设置Content-Type
    std::string::size_type idx = filePath.find_last_of('.');
    if (idx == std::string::npos) {
      resp->setContentType("text/plain");
    } else {
      const std::string suffix = filePath.substr(idx);
      auto it = HttpResponse::SUFFIX_TYPE.find(suffix);
      if (it != HttpResponse::SUFFIX_TYPE.end()) {
        resp->setContentType(it->second);
      } else {
        resp->setContentType("text/plain");
      }
    }
    char *mmFile = static_cast<char *>(mmRet);
    const std::string body(mmFile, mmFileStat.st_size);
    resp->setBody(body);
  }
}