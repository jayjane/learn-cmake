#ifndef SRC_WEB_CONFIG_H
#define SRC_WEB_CONFIG_H

#include <muduo/base/Logging.h>

#include <string>

namespace webserver {

class Config {
public:
  Config() = default;
  ~Config() = default;

  std::string resourcesPath_;
  std::string serveName_;
  uint16_t port_;
  bool log_;
  muduo::Logger::LogLevel logLevel_;
  int threadNum_;
}; // end of class Config

} // namespace webserver

#endif // !SRC_WEB_CONFIG_H
