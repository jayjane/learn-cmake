#ifndef SRC_WEB_WEBSERVER_H
#define SRC_WEB_WEBSERVER_H

#include "src/http/HttpRequest.h"
#include "src/http/HttpResponse.h"
#include "src/http/HttpServer.h"
#include "src/web/Config.h"

#include <muduo/base/Logging.h>
#include <muduo/net/EventLoop.h>

#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <regex>

using namespace muduo;
using namespace muduo::net;
using namespace webserver;
using namespace webserver::http;

namespace webserver {

class Webserver {
public:
  using Handler = std::function<void(const http::HttpRequest &req,
                                     http::HttpResponse *resp)>;

public:
  Webserver(const Config &config);
  ~Webserver() = default;

  void start();

  void post(const std::string path, Handler handler);
  void get(const std::string path, Handler handler);

private:
  using pathTOmethod = std::pair<std::string, std::string>;

  void onRequest(const http::HttpRequest &req, http::HttpResponse *resp);
  void defaultOnRequest(const http::HttpRequest &req, http::HttpResponse *resp);

  Handler defaultHandler;
  std::unique_ptr<muduo::net::EventLoop> loop_;
  std::unique_ptr<webserver::http::HttpServer> httpServer_;
  std::map<pathTOmethod, Handler> handlers_;
}; // end of class webserver

} // namespace webserver

#endif // !SRC_WEB_WEBSERVER_H