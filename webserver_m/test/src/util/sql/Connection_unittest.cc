#include "src/util/sql/Connection.h"

#include <gtest/gtest.h>

using namespace webserver::util::sql;

TEST(testJustConnect, testJustConnect) {
  Connection conn;
  EXPECT_TRUE(conn.connect("localhost", 3306, "root", "498756", "yourdb"));
}

/**
 * @brief 测试MySQL连接的更新功能
 * 
 */
TEST(testUpdate, testUpdate) {
  Connection conn;
  EXPECT_TRUE(conn.connect("localhost", 3306, "root", "498756", "yourdb"));
  const std::string sql =
      R"(UPDATE user SET username="jiandawei", password="498756")";
  EXPECT_TRUE(conn.update(sql));
}

/**
 * @brief 测试MySQL连接的查询功能
 * 
 */
TEST(testQuery, testQuery) {
  Connection conn;
  EXPECT_TRUE(conn.connect("localhost", 3306, "root", "498756", "yourdb"));
  const std::string sql =
      R"(SELECT username, password FROM user where username="jiandawei";)";
  MYSQL_RES *res = nullptr;
  res = conn.query(sql);
  EXPECT_TRUE(res != nullptr);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}