#include "src/util/sql/ConnectionPool.h"

#include <muduo/base/Logging.h>

#include <ctime>
#include <thread>
#include <vector>

using namespace webserver::util::sql;
using namespace std;

const std::string querrySql =
    R"(SELECT username, password FROM user where username="jiandawei";)";
const std::string updateSql =
    R"(UPDATE user SET username="jiandawei", password="498756")";

vector<int> data_size{1000, 10000, 100000};  //数据量
vector<int> num_thread{1, 2, 4, 8, 16, 32};           // 线程数量

int main() {
  clock_t begin, end;
  for (int n : data_size) {       // {1000, 10000, 100000}
    for (int num : num_thread) {  // {1, 2, 4, 8, 16, 32}
      // 不使用连接池
      {
        vector<thread> threads;
        begin = clock();
        for (int i = 0; i < num; i++) {
          threads.push_back(thread([&]() {
            for (int j = 0; j < n / num; ++j) {
              Connection conn;
              conn.connect("localhost", 3306, "root", "498756", "yourdb");
              conn.update(updateSql);
              auto res = conn.query(querrySql);
              mysql_free_result(res);
            }
          }));
        }
        for (auto& t : threads) {
          t.join();
        }
        end = clock();
        LOG_INFO << "data size: " << n << ",use " << num
                 << " thread without pool cost: " << end - begin << "ms";
      }

      // 使用连接池
      {
        vector<thread> threads;
        begin = clock();
        ConnectionPool& g_ConnectionPool = ConnectionPool::getConnectionPool();
        for (int i = 0; i < num; i++) {
          threads.push_back(thread([&]() {
            for (int j = 0; j < n / num; ++j) {
              auto conn = g_ConnectionPool.getConnection();
              conn->update(updateSql);
              auto res = conn->query(querrySql);
              mysql_free_result(res);
            }
          }));
        }
        for (auto& t : threads) {
          t.join();
        }
        end = clock();
        LOG_INFO << "data size: " << n << ",use " << num
                 << " thread with pool cost:    " << end - begin << "ms";
      }
    }
  }

  return 0;
}