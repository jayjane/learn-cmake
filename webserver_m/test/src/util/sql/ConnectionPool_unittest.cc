#include "src/util/sql/ConnectionPool.h"

#include <gtest/gtest.h>

using namespace webserver::util::sql;
using namespace std;

// 获取数据库连接池全局对象
ConnectionPool &g_ConnectionPool = ConnectionPool::getConnectionPool();

// dbconf.json内容如下：
/*
{
    "ip": "localhost",
    "port": 3306,
    "user": "root",
    "pwd": "498756",
    "minSize": 8,
    "maxSize": 16,
    "maxIdleTime": 60,
    "timeout": 60,
    "db": "yourdb"
}
*/
TEST(testLoadConfig, testLoadConfig) {
  EXPECT_STREQ("localhost", g_ConnectionPool.getIP().data());
  EXPECT_EQ(3306, g_ConnectionPool.getPort());
  EXPECT_STREQ("root", g_ConnectionPool.getUserName().data());
  EXPECT_STREQ("498756", g_ConnectionPool.getUserPassword().data());
  EXPECT_EQ(8, g_ConnectionPool.getMinSize());
  EXPECT_EQ(16, g_ConnectionPool.getMaxSize());
  EXPECT_EQ(60, g_ConnectionPool.getMaxIdleTime());
  EXPECT_EQ(60, g_ConnectionPool.getConnectionTimeout());
  EXPECT_STREQ("yourdb", g_ConnectionPool.getDataBaseName().data());
}

TEST(testConnSize, testConnSize) {
  EXPECT_EQ(8, g_ConnectionPool.getCurConnCnt());
}

TEST(testUpdate, testUpdate) {
  auto conn = g_ConnectionPool.getConnection();
  const std::string sql =
      R"(UPDATE user SET username="jiandawei", password="498756")";
  EXPECT_TRUE(conn->update(sql));
}

TEST(testQuery, testQuery) {
  auto conn = g_ConnectionPool.getConnection();
  const std::string sql =
      R"(SELECT username, password FROM user where username="jiandawei";)";
  MYSQL_RES *res = nullptr;
  res = conn->query(sql);
  EXPECT_TRUE(res != nullptr);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}