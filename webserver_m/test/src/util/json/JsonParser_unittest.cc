#include "src/util/json/JsonParser.h"

#include <string>
#include <iostream>
#include <gtest/gtest.h>

using namespace webserver::util::json;
using namespace std;

TEST(testParseEmpty, testParseEmpty) {
  const std::string json = "";
  wJson value = JsonParser::parse(json);
  EXPECT_TRUE(value.empty());
}

TEST(testParseSimple, testParseSimple) {
  const std::string json = R"({
                                "name":"菜鸟教程",
                                "url":"www.runoob.com"
                              })";
  wJson value = JsonParser::parse(json);
  EXPECT_TRUE(!value.empty());
  EXPECT_TRUE(value["name"].isString());
  EXPECT_STREQ(value["name"].asCString(), "菜鸟教程");
  EXPECT_TRUE(value["url"].isString());
  EXPECT_STREQ(value.get("url", "Empty URL").asCString(), "www.runoob.com");
  EXPECT_TRUE(value["NoSuchKey"].empty());
}

TEST(testParseArray, testParseArray) {
  const std::string json = R"({
    "sites": [
        { "name":"菜鸟教程" , "url":"www.runoob.com" },
        { "name":"google" , "url":"www.google.com" },
        { "name":"微博" , "url":"www.weibo.com" }
    ]
  })";

  wJson value = JsonParser::parse(json);
  wJson sites = value["sites"];

  EXPECT_TRUE(!sites.empty());
  EXPECT_STREQ(sites[0]["name"].asCString(), "菜鸟教程");
  EXPECT_STREQ(sites[0]["url"].asCString(), "www.runoob.com");
  EXPECT_STREQ(sites[1]["name"].asCString(), "google");
  EXPECT_STREQ(sites[1]["url"].asCString(), "www.google.com");
  EXPECT_STREQ(sites[2]["name"].asCString(), "微博");
  EXPECT_STREQ(sites[2]["url"].asCString(), "www.weibo.com");
}

TEST(testParseNumber, testParseNumber) {
    const std::string json = R"(
        {
            "year": 1234,
            "month": 56,
            "day": 78,
            "hour": 90,
            "min": 12,
            "sec": 34,
            "nsec": 56.78
        }
    )";
    wJson value = JsonParser::parse(json);
    EXPECT_TRUE(value["year"].isInt());
    EXPECT_EQ(value["year"], 1234);
    EXPECT_TRUE(value["month"].isInt());
    EXPECT_EQ(value["month"], 56);
    EXPECT_TRUE(value["day"].isInt());
    EXPECT_EQ(value["day"], 78);
    EXPECT_TRUE(value["hour"].isInt());
    EXPECT_EQ(value["hour"], 90);
    EXPECT_TRUE(value["min"].isInt());
    EXPECT_EQ(value["min"], 12);
    EXPECT_TRUE(value["sec"].isInt());
    EXPECT_EQ(value["sec"], 34);
    EXPECT_TRUE(value["nsec"].isDouble());
    EXPECT_EQ(value["nsec"], 56.78);
}

TEST(testParseMix, testParseMix) {
    const std::string json = R"(
        {
            "sites": {
                "site": [
                {
                    "id": "1",
                    "name": "菜鸟教程",
                    "url": "www.runoob.com"
                },
                {
                    "id": "2",
                    "name": "菜鸟工具",
                    "url": "c.runoob.com"
                }
                ]
            },
            "time": {
                "year": 1234,
                "nsec": 56.78
            }
        }
    )";
    wJson value = JsonParser::parse(json);
    
    wJson sites = value["sites"]["site"];
    EXPECT_STREQ(sites[0]["id"].asCString(), "1");
    EXPECT_STREQ(sites[0]["name"].asCString(), "菜鸟教程");
    EXPECT_STREQ(sites[0]["url"].asCString(), "www.runoob.com");
    EXPECT_STREQ(sites[1]["id"].asCString(), "2");
    EXPECT_STREQ(sites[1]["name"].asCString(), "菜鸟工具");
    EXPECT_STREQ(sites[1]["url"].asCString(), "c.runoob.com");
    
    wJson time = value["time"];
    EXPECT_TRUE(time["year"].isInt());
    EXPECT_EQ(time["year"], 1234);
    EXPECT_TRUE(time["nsec"].isDouble());
    EXPECT_EQ(time["nsec"], 56.78);
}

/*
/root/cmake-demo/webserver_m/test/src/util/json/dbconf.json文件内容：
{
    "ip": "localhost",
    "port": 3306,
    "user": "root",
    "pwd": "498756",
    "minSize": 8,
    "maxSize": 16,
    "maxIdleTime": 60,
    "timeout": 60
}
*/
TEST(testParseFromFile, testParseFromFile) {
    wJson config = JsonParser::parseFromFile("/root/cmake-demo/webserver_m/test/src/util/json/dbconf.json");
    EXPECT_STREQ(config["ip"].asCString(), "localhost");
    EXPECT_TRUE(config["port"].isInt());
    EXPECT_EQ(config["port"], 3306);
    EXPECT_STREQ(config["user"].asCString(), "root");
    EXPECT_STREQ(config["pwd"].asCString(), "498756");
    EXPECT_TRUE(config["minSize"].isInt());
    EXPECT_EQ(config["minSize"], 8);
    EXPECT_TRUE(config["maxSize"].isInt());
    EXPECT_EQ(config["maxSize"], 16);
    EXPECT_TRUE(config["maxIdleTime"].isInt());
    EXPECT_EQ(config["maxIdleTime"], 60);
    EXPECT_TRUE(config["timeout"].isInt());
    EXPECT_EQ(config["timeout"], 60);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}