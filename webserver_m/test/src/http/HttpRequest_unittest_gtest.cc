#include "muduo/net/Buffer.h"
#include "src/http/HttpContext.h"

#include <gtest/gtest.h>

using muduo::Timestamp;
using muduo::net::Buffer;
using webserver::http::HttpContext;
using webserver::http::HttpRequest;

TEST(testParseRequestAllInOne, testParseRequestAllInOne) {
  HttpContext context;
  Buffer input;
  input.append("GET /index.html HTTP/1.1\r\n"
               "Host: www.chenshuo.com\r\n"
               "\r\n");

  EXPECT_TRUE(context.parseRequest(&input, Timestamp::now()));
  EXPECT_TRUE(context.gotAll());

  const HttpRequest &request = context.request();
  EXPECT_EQ(request.method(), HttpRequest::kGet);
  EXPECT_STREQ(request.getPath().c_str(), "/index.html");
  EXPECT_EQ(request.getVersion(), HttpRequest::kHttp11);
  EXPECT_STREQ(request.getHeader("Host").c_str(), "www.chenshuo.com");
  EXPECT_STREQ(request.getHeader("User-Agent").c_str(), "");
}

TEST(testParseRequestInTwoPieces, testParseRequestInTwoPieces) {
  std::string all("GET /index.html HTTP/1.1\r\n"
             "Host: www.chenshuo.com\r\n"
             "\r\n");

  for (size_t sz1 = 0; sz1 < all.size(); ++sz1) {
    HttpContext context;
    Buffer input;
    input.append(all.c_str(), sz1);
    EXPECT_TRUE(context.parseRequest(&input, Timestamp::now()));
    EXPECT_TRUE(!context.gotAll());

    size_t sz2 = all.size() - sz1;
    input.append(all.c_str() + sz1, sz2);
    EXPECT_TRUE(context.parseRequest(&input, Timestamp::now()));
    EXPECT_TRUE(context.gotAll());

    const HttpRequest &request = context.request();
    EXPECT_EQ(request.method(), HttpRequest::kGet);
    EXPECT_STREQ(request.getPath().c_str(), "/index.html");
    EXPECT_EQ(request.getVersion(), HttpRequest::kHttp11);
    EXPECT_STREQ(request.getHeader("Host").c_str(), "www.chenshuo.com");
    EXPECT_STREQ(request.getHeader("User-Agent").c_str(), "");
  }
}

TEST(testParseRequestEmptyHeaderValue, testParseRequestEmptyHeaderValue) {
  HttpContext context;
  Buffer input;
  input.append("GET /index.html HTTP/1.1\r\n"
               "Host: www.chenshuo.com\r\n"
               "User-Agent:\r\n"
               "Accept-Encoding: \r\n"
               "\r\n");

  EXPECT_TRUE(context.parseRequest(&input, Timestamp::now()));
  EXPECT_TRUE(context.gotAll());

  const HttpRequest &request = context.request();
  EXPECT_EQ(request.method(), HttpRequest::kGet);
  EXPECT_STREQ(request.getPath().c_str(), "/index.html");
  EXPECT_EQ(request.getVersion(), HttpRequest::kHttp11);
  EXPECT_STREQ(request.getHeader("Host").c_str(), "www.chenshuo.com");
  EXPECT_STREQ(request.getHeader("User-Agent").c_str(), "");
  EXPECT_STREQ(request.getHeader("Accept-Encoding").c_str(), "");
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}