#!/bin/sh

set -x
SOURCE_DIR=`pwd`                    # 将当前目录记为SOURCE_DIR
BUILD_DIR=${BUILD_DIR:-./build}     # 编译构建目录为当前目录下的build目录
BUILD_TYPE=${BUILD_TYPE:-release}   # 编译类型为release
# INSTALL_DIR=${INSTALL_DIR:-../${BUILD_TYPE}-install-cpp11} # 安装目录为../release-install--cpp11
CXX=${CXX:-g++} # 编译器为g++

mkdir -p $BUILD_DIR/$BUILD_TYPE-cpp11 \
  && cd $BUILD_DIR/$BUILD_TYPE-cpp11 \
  && cmake \
           -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
           -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
           -DCMAKE_EXPORT_COMPILE_COMMANDS=OFF \
           $SOURCE_DIR \
  && make $* 
# && make install