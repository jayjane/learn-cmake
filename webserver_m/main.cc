#include "src/web/Webserver.h"

using namespace webserver;

Config config;
config.log_ = true;
config.logLevel_ = muduo::Logger::INFO;
config.port_ = 8000;
config.threadNum_ = 6;
config.serveName_ = "WebServer";
config.resourcesPath_ = "/root/cmake-demo/webserver_m/resources";

Webserver webserver(config);

webserver.post("/chuan", [](const http::HttpRequest &req,
                            http::HttpResponse *resp) {
  util::json::wJson nullValue;
  util::json::wJson jsonData = req.getJsonData();
  if (!jsonData["num"].isNull()) {
    LOG_INFO << "/chuan recieve an number " << jsonData["num"].asString();
    resp->setStatusCode(HttpResponse::HttpStatusCode::k200Ok);
    resp->setStatusMessage("OK");
    const std::string body = "OK, Server recieve the number";
    resp->setBody(body);
    resp->setContentType("text/plain");
  }
});

int main() { webserver.start(); }