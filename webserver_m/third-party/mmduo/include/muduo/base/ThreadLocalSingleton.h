#ifndef MUDUO_BASE_THREADLOCAL_H
#define MUDUO_BASE_THREADLOCAL_H

#include "muduo/base/noncopyable.h"
#include <assert.h>
#include <pthread.h>

namespace muduo {

// 这个类和Singleton不同之处，没有使用ptread_once。而是使用内部类Deleter static
// 成员变量 deleter_ 来操作pthread_key
// 使得ThreadLocalSingleton专注于t_value_的管理。虽然__thread
// 可以修饰POD类型变量，这里是指针，所以是可以的

/**
 * 这个类是TLS单例，是每个线程各一份。在每个线程中是单例。
 * 而不是所有线程共享一份。
 * 这个类相当于Signleton<ThreadLocal<T>>
 */
template <typename T> class ThreadLocalSingleton : public noncopyable {
public:
  ThreadLocalSingleton() = delete;
  ~ThreadLocalSingleton() = delete;

  static T &instance() {
    if (!t_value_) {
      t_value_ = new T();
      deleter_.set(t_value_);
    }
    return *t_value_;
  }

  static T *pointer() { return t_value_; }

private:
  static void destructor(void *obj) {
    assert(obj == t_value_);
    typedef char T_must_be_complete_type[sizeof(T) == 0 ? -1 : 1];
    T_must_be_complete_type dummy;
    (void)dummy;

    delete t_value_;
    t_value_ = NULL;
  }

private:
  class Deleter {
  public:
    Deleter() { pthread_key_create(&pkey_, &ThreadLocalSingleton::destructor); }

    ~Deleter() { pthread_key_delete(pkey_); }

    void set(void *newObj) {
      assert(pthread_getspecific(pkey_) == NULL);
      pthread_setspecific(pkey_, newObj);
    }

  private:
    pthread_key_t pkey_;
  }; // end of class Deleter

private:
  static __thread T *t_value_;
  static Deleter deleter_;
}; // end of class ThreadLocalSingleton

// 静态成员变量初始化
template <typename T> __thread T *ThreadLocalSingleton<T>::t_value_ = NULL;

template <typename T>
typename ThreadLocalSingleton<T>::Deleter ThreadLocalSingleton<T>::deleter_;

}; // end of namespace muduo

#endif // !MUDUO_BASE_THREADLOCAL_H