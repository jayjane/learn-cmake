#ifndef MUDUO_BASE_SINGLETON_H
#define MUDUO_BASE_SINGLETON_H

#include "muduo/base/noncopyable.h"
#include <assert.h>
#include <pthread.h>
#include <stdlib.h> // atexit

namespace muduo {

namespace detail {
// This doesn't detect inherited member functions!
// http://stackoverflow.com/questions/1966362/sfinae-to-check-for-inherited-member-functions
template <typename T> struct has_no_destroy {
  // 当T有no_destroy成员时，会匹配的第一个函数，sizeof(test<T>(0)) 会等于1
  // 即has_no_destroy<T>::value 为true时，T有no_destroy成员(变量或函数)
  template <typename C> static char test(decltype(&C::no_destroy));
  template <typename C> static int32_t test(...);
  const static bool value = sizeof(test<T>(0)) == 1;
}; // end of struct has_no_destroy

} // namespace detail

template <typename T> class Singleton : public noncopyable {
public:
  Singleton() = delete;
  ~Singleton() = delete;

  static T &instance() {
    pthread_once(&ponce_, &Singleton::init);
    assert(value_ != NULL);
    return *val_;
  }

private:
  static void init() {
    val_ = new T();
    if (!detail::has_no_destroy<T>::value) {
      ::atexit(destory);
    }
  }

  static void destory() {
    // 如果T为incomplete type会在编译阶段报错。error: invalid application of
    // ‘sizeof’ to incomplete type
    typedef char T_must_be_complete_type[sizeof(T) == 0 ? -1 : 1];
    T_must_be_complete_type dummy;
    (void)dummy; // 避免unused报错

    delete val_;
    val_ = nullptr;
  }

private:
  static pthread_once_t ponce_;
  static T *val_;
}; // end of class Singleton

// 初始化静态成员变量
template <typename T> T *Singleton<T>::val_ = nullptr;

template <typename T> pthread_once_t Singleton<T>::ponce_ = PTHREAD_ONCE_INIT;

}; // end of namespace muduo

#endif // !MUDUO_BASE_SINGLETON_H