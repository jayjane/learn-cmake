#ifndef MUDUO_BASE_NONCOPYABLE_H
#define MUDUO_BASE_NONCOPYABLE_H

namespace muduo {

class noncopyable {
public:
  // delete the default copy ctor and assign operator(=)
  noncopyable(const noncopyable &) = delete;
  void operator=(const noncopyable &) = delete;

protected:
  noncopyable() = default;
  ~noncopyable() = default;
}; // end of class noncopyable

}; // namespace muduo

#endif // !MUDUO_BASE_NONCOPYABLE_H