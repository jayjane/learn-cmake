#ifndef MUDUO_BASE_BOUNDEDBLOCKINGQUEUE_H
#define MUDUO_BASE_BOUNDEDBLOCKINGQUEUE_H

#include "muduo/base/noncopyable.h"

#include <assert.h>
#include <boost/circular_buffer.hpp>
#include <condition_variable>
#include <mutex>

namespace muduo {

template <typename T> class BoundedBlockingQueue : public noncopyable {
public:
  explicit BoundedBlockingQueue(int maxSize)
      : mtx_(), notEmpty_(), notFull_(), queue_(maxSize) {}

  void put(const T &t) {
    std::unique_lock<std::mutex> lock(mtx_);
    while (queue_.full()) {
      notFull_.wait(lock);
    }
    assert(!queue_.full());
    queue_.push_back(t);
    notEmpty_.notify_one();
  }

  void put(T &&t) {
    std::unique_lock<std::mutex> lock(mtx_);
    while (queue_.full()) {
      notFull_.wait(lock);
    }
    assert(!queue_.full());
    queue_.push_back(std::move(t));
    notEmpty_.notify_one();
  }

  T take() {
    std::unique_lock<std::mutex> lock(mtx_);
    while (queue_.empty()) {
      notEmpty_.wait(lock);
    }
    assert(!queue_.empty());
    T t(std::move(queue_.front()));
    queue_.pop_front();
    notFull_.notify_one();
    return t;
  }

  // 将阻塞队列的所有元素移到另一个队列(deque)
  std::deque<T> drain() {
    std::deque<T> queue;
    {
      std::lock_guard<std::mutex> lock(mtx_);
      queue = std::move(queue_);
      assert(queue_.empty());
    }
    return queue;
  }

  bool empty() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.empty();
  }

  bool full() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.full();
  }

  size_t size() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.size();
  }

  size_t capacity() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.capacity();
  }

private:
  std::mutex mtx_;
  std::condition_variable notEmpty_;
  std::condition_variable notFull_;
  boost::circular_buffer<T> queue_;
}; // end of class BoundedBlockingQueue

}; // end of namespace muduo

#endif // !MUDUO_BASE_BOUNDEDBLOCKINGQUEUE_H