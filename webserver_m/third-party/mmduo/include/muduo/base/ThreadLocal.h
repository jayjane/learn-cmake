#ifndef MUDUO_BASE_THREADLOCAL_H
#define MUDUO_BASE_THREADLOCAL_H

#include "muduo/base/noncopyable.h"
#include <pthread.h>

namespace muduo {

template <typename T> class ThreadLocal : public noncopyable {
public:
  ThreadLocal() {
    // 创建key，并指定删除key所指数据的回调函数
    pthread_key_create(&pkey_, &ThreadLocal::destructor);
  }

  ~ThreadLocal() {
    // 这里只是删除pkey_, 不是删除其指向的数据
    pthread_key_delete(pkey_);
  }

  T &value() {
    T *perThreadValue = static_cast<T *>(pthread_getspecific(pkey_));
    if (!perThreadValue) {
      perThreadValue = new T();
      pthread_setspecific(pkey_, perThreadValue);
    }
    return *perThreadValue;
  }

private:
  static void destructor(void *x) {
    T *obj = static_cast<T *>(x);
    typedef char T_must_be_complete_type[sizeof(T) == 0 ? -1 : 1];
    T_must_be_complete_type dummy;
    (void)dummy;
    delete obj;
  }

private:
  pthread_key_t pkey_;
}; // end of class ThreadLocal

}; // end of namespace muduo

#endif // !MUDUO_BASE_THREADLOCAL_H