#ifndef MUDUO_BASE_BLCOKINGQUEUE_H
#define MUDUO_BASE_BLCOKINGQUEUE_H

#include "muduo/base/noncopyable.h"

#include <assert.h>
#include <condition_variable>
#include <deque>
#include <mutex>

namespace muduo {

template <typename T> class BlockingQueue : public noncopyable {
public:
  explicit BlockingQueue() : mtx_(), notEmpty_(), queue_() {}

  void put(const T &t) {
    std::lock_guard<std::mutex> lock(mtx_);
    queue_.push_back(t);
    notEmpty_.notify_one();
  }

  // &&：右值引用
  void put(T &&t) {
    std::lock_guard<std::mutex> lock(mtx_);
    // queue_.push_back(t);
    /*
    这里一定要用std::move(),
    否则BlockingQueue_test中的testMove()会报错：说用到了这个函数unique_ptr(const
    unique_ptr&) = delete;
    那里用到传入的参数是右值引用，所以会调用现在这个函数。Todo：why?
    */
    queue_.push_back(std::move(t));
    notEmpty_.notify_one();
  }

  T take() {
    std::unique_lock<std::mutex> lock(mtx_);
    while (queue_.empty()) {
      notEmpty_.wait(lock);
    }
    assert(!queue_.empty());
    T t(std::move(queue_.front()));
    queue_.pop_front();
    return t;
  }

  // 将阻塞队列的所有元素移到另一个队列(deque)
  std::deque<T> drain() {
    std::deque<T> queue;
    {
      std::lock_guard<std::mutex> lock(mtx_);
      queue = std::move(queue_);
      assert(queue_.empty());
    }
    return queue;
  }

  size_t size() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.size();
  }

private:
  std::mutex mtx_;
  std::condition_variable notEmpty_;
  std::deque<T> queue_;
}; // end of class BlockingQueue

}; // end of namespace muduo

#endif // !MUDUO_BASE_BLCOKINGQUEUE_H