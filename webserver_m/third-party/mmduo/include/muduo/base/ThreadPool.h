#ifndef MUDUO_BASE_THREADPOOL_H
#define MUDUO_BASE_THREADPOOL_H

#include "muduo/base/Thread.h"
#include "muduo/base/Types.h"

#include <condition_variable>
#include <deque>
#include <functional>
#include <memory>
#include <mutex>
#include <vector>

namespace muduo {

class ThreadPool {
public:
  using Task = std::function<void()>;

  explicit ThreadPool(const std::string &name = std::string());

  ~ThreadPool();

  void setMaxQueueSize(int maxSize) { maxQueueSize_ = maxSize; }
  void setThreadInitCallback(const Task &cb) { threadInitCallback_ = cb; }

  void start(int numThreads);
  void stop();

  const std::string &name() const { return name_; }

  size_t queueSize() {
    std::lock_guard<std::mutex> lock(mtx_);
    return queue_.size();
  }

  bool isFull() {
    // 这里不能加锁，因为在run函数内，会先加锁，在调用isFull，这里如果再加锁，就会造成死锁
    // std::lock_guard<std::mutex> lock(mtx_);
    return maxQueueSize_ > 0 &&
           queue_.size() >= static_cast<std::size_t>(maxQueueSize_);
  }

  void run(Task f);

  Task take();

private:
  void runInThread();
  std::mutex mtx_;
  std::condition_variable notEmpty_;
  std::condition_variable notFull_;
  int maxQueueSize_;
  std::string name_;
  Task threadInitCallback_;
  std::vector<std::unique_ptr<muduo::Thread>> threads_;
  std::deque<Task> queue_;
  bool running_;
}; // end of class ThreadPool

}; // end of namespace muduo

#endif // !MUDUO_BASE_THREADPOOL_H