#ifndef MUDUO_NET_TCPCONNECTION_H
#define MUDUO_NET_TCPCONNECTION_H

#include "muduo/base/StringPiece.h"
#include "muduo/base/Types.h"
#include "muduo/base/noncopyable.h"
#include "muduo/net/Buffer.h"
#include "muduo/net/Callbacks.h"
#include "muduo/net/InetAddress.h"

#include <netinet/tcp.h> // struct tcp_info; //is in <netinet/tcp.h>

#include <boost/any.hpp>
#include <memory>

namespace muduo {
namespace net {

class Channel;
class EventLoop;
class Socket;

///
/// TCP connection, for both client and server usage.
///
/// This is an interface class, so don't expose too much details.

// std::enable_shared_from_this<T>是一个模板类，
// 使得在T的成员函数中调用share_from_this生成一个share_ptr<T>
class TcpConnection : public noncopyable,
                      public std::enable_shared_from_this<TcpConnection> {
public:
  /// Constructs a TcpConnection with a connected sockfd
  ///
  /// User should not create this object.
  TcpConnection(EventLoop *loop, const std::string &name, int sockfd,
                const InetAddress &localAddr, const InetAddress &peerAddr);
  ~TcpConnection();

  EventLoop *getLoop() const { return loop_; }
  const std::string &name() const { return name_; }
  const InetAddress &localAddress() const { return localAddr_; }
  const InetAddress &peerAddress() const { return peerAddr_; }
  bool connected() const { return state_ == kConnected; }
  bool disconnected() const { return state_ == kDisconnected; }

  bool getTcpInfo(struct tcp_info *) const;
  std::string getTcpInfoToString() const;

  // send函数都是线程安全的
  void send(const void *message, int len);
  void send(const StringPiece &message);
  void send(Buffer *message);

  // 主要是由于state_不是原子型导致的线程不安全
  void shutdown(); // NOT thread safe, no simultaneous calling

  void forceClose();
  void forceCloseWithDelay(double seconds);
  void setTcpNoDelay(bool on);

  // reading or not
  void startRead();
  void stopRead();
  bool isReading() const {
    return reading_;
  }; // NOT thread safe, may race with start/stopReadInLoop

  void setContext(const boost::any &context) { context_ = context; }
  const boost::any &getContext() const { return context_; }
  boost::any *getMutableContext() { return &context_; }

  // 设置连接建立回调函数，用户不直接调用，由TcpServer::newConnection调用
  void setConnectionCallback(const ConnectionCallback &cb) {
    connectionCallback_ = cb;
  }

  // 设置消息到来回调函数，用户不直接调用，由TcpServer::newConnection调用
  void setMessageCallback(const MessageCallback &cb) { messageCallback_ = cb; }

  // 设置写完毕回调函数
  void setWriteCompleteCallback(const WriteCompleteCallback &cb) {
    writeCompleteCallback_ = cb;
  }

  // 设置高水位回调函数
  void setHighWaterMarkCallback(const HighWaterMarkCallback &cb,
                                size_t highWaterMark) {
    highWaterMarkCallback_ = cb;
    highWaterMark_ = highWaterMark;
  }

  /// Internal use only.
  void setCloseCallback(const CloseCallback &cb) { closeCallback_ = cb; }

  /// Advanced interface
  Buffer *inputBuffer() { return &inputBuffer_; }

  Buffer *outputBuffer() { return &outputBuffer_; }

  // called when TcpServer accepts a new connection
  void connectEstablished(); // should be called only once
  // called when TcpServer has removed me from its map
  void connectDestroyed(); // should be called only once

private:
  // TCP连接的状态：连接已断开、正在建立连接、已建立连接、正在断开连接
  enum StateE { kDisconnected, kConnecting, kConnected, kDisconnecting };
  void handleRead(Timestamp receiveTime);
  void handleWrite();
  void handleClose();
  void handleError();

  void sendInLoop(const StringPiece &message);
  void sendInLoop(const void *message, size_t len);
  void shutdownInLoop();

  void forceCloseInLoop();
  void setState(StateE s) { state_ = s; }
  const char *stateToString() const;
  void startReadInLoop();
  void stopReadInLoop();

private:
  EventLoop *loop_;   // 所属EventLoop
  const string name_; // TCP连接的名字
  StateE state_;      // TCP连接的状态。 FIXME：use atomic variable
  bool reading_;      // 是否正在读
  std::unique_ptr<Socket> socket_;   // TCP连接的socket
  std::unique_ptr<Channel> channel_; // 监听TCP连接的Channel
  const InetAddress localAddr_;      // TCP连接的本端地址
  const InetAddress peerAddr_;       // TCP连接的对端地址

  // 回调函数
  ConnectionCallback connectionCallback_; // 这个回调函数是来自TcpServer
  MessageCallback messageCallback_; // 这个回调函数是来自TcpServer
  
  /**
   * 数据发送完毕回调函数，即数据从应用层缓冲区拷贝到内核应用缓冲区。
   * outputBuffer_被清空也会对调该函数，可以理解为低水位标回调函数。
   * */ 
  /**
   * 大流量应用场景下：不断生成数据，然后发送数据（conn->send()）.
   * 如果对等方接收不及时，受到通知窗口的控制，导致内核发送缓冲区空间不足，
   * 数据会堆积在应用层输出缓冲区（outputBuffer），可能会撑爆outputBuffer。
   * 解决方法：调整发送频率，关注WriteCompleteCallback，所有的数据都发送完，
   * WriteCompleteCallback回调，然后继续发送。
   * 而在低流量应用场景下：不需要关注WriteCompleteCallback。 
  */
  WriteCompleteCallback writeCompleteCallback_; 
  /**
   * 高水位标回调函数。outputBuffer数据多到一定程度时
   * （数据量大于高水位标），调用该回调函数
   * */ 
  HighWaterMarkCallback highWaterMarkCallback_;  
  CloseCallback closeCallback_;

  size_t highWaterMark_; // Todo：高水位标志
                         // FIXME: use list<Buffer> as output buffer.
  Buffer inputBuffer_; // TCP连接的输入缓冲区，存储从TCP连接socket读的数据
  Buffer outputBuffer_; // TCP连接的输出缓冲区，存储要往TCP连接socket写的数据
  boost::any context_; // 绑定一个未知类型的上下文对象

  // FIXME: creationTime_, lastReceiveTime_
  //        bytesReceived_, bytesSent_
}; // end of class TcpConnection

// Todo: 为什么要用share_ptr
using TcpConnectionPtr = std::shared_ptr<TcpConnection>;

}; // end of namespace net
}; // end of namespace muduo

#endif // MUDUO_NET_TCPCONNECTION_H