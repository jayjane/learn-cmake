#ifndef MUDUO_NET_ENDIAN_H
#define MUDUO_NET_ENDIAN_H

#include <endian.h>
#include <stdint.h>

namespace muduo {

namespace net {

namespace sockets {

// 在GCC下，#pragma GCC diagnostic push用于记录当前的诊断状态，
// #pragma GCC diagnostic pop用于恢复诊断状态。

// 由于下面的函数会用到c-style类型转换，
// 所以使下面两个编译选项暂时失效
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wold-style-cast"

// 注意以下的字节序转换的函数都是无符号整数
inline uint64_t hostToNetwork64(uint64_t host64) { return htobe64(host64); }

inline uint32_t hostToNetwork32(uint32_t host32) { return htobe32(host32); }

inline uint16_t hostToNetwork16(uint64_t host16) { return htobe16(host16); }

inline uint64_t networkToHost64(uint64_t net64) { return be64toh(net64); }

inline uint32_t networkToHost32(uint32_t net32) { return be32toh(net32); }

inline uint16_t networkToHost16(uint16_t net16) { return be16toh(net16); }

#pragma GCC diagnostic pop

}; // end of namespace sockets

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_ENDIAN_H
