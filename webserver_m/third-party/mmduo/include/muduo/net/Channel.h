#ifndef MUDUO_NET_CHANNEL_H
#define MUDUO_NET_CHANNEL_H

#include "muduo/base/noncopyable.h"
#include "muduo/base/Timestamp.h"

#include <functional> // function
#include <memory>     // weak_ptr, shared_ptr

namespace muduo {

namespace net {

// 前置声明，没有包含头文件,
// 因为EventLoop.h包含了这个头文件
class EventLoop;

class Channel : public noncopyable {
public:
  using EventCallback = std::function<void()>;
  using ReadEventCallback = std::function<void(Timestamp timestamp)>;

public:
  Channel(EventLoop *loop, int fd);
  ~Channel();

  // 当Channel有关心的事件就绪时, EventLoop就回调用Channel::handleEvent(),
  // Channel::handleEvent()又会调用Channel::handleEventWithGuard()
  void handleEvent(Timestamp receiveTime);

  // 设置回调函数
  void setReadCallback(ReadEventCallback cb) { readCallback_ = std::move(cb); }
  void setWriteCallback(EventCallback cb) { writeCallback_ = std::move(cb); }
  void setErrorCallback(EventCallback cb) { errorCallback_ = std::move(cb); }
  void setCloseCallback(EventCallback cb) { closeCallback_ = std::move(cb); }

  // 将该Channel与obj所指对象绑定，
  // 避免该对象在handleEvent函数执行期间被析构，出现core dump
  // 这个绑定的对象就是TcpConnection
  void tie(const std::shared_ptr<void> &obj);

  int fd() const { return fd_; }
  int events() const { return events_; }
  void setRevents(int revt) {
    revents_ = revt;
  } // 使用Poller用于更新Channel所发生的事件
  bool isNoneEvent() const { return events_ == kNoneEvent; }

  // 注意disable是用 &= ~
  // events_ = 0000, POLLIN = 0010
  // enableReading就是events_ |= POLLIN, events_变成0010
  // disableReading就是events_ &~= POLLIN, events_就变会0000
  void enableReading() {
    events_ |= KReadEvent;
    update();
  }
  void disableReading() {
    events_ &= ~KReadEvent;
    update();
  }
  void enableWriting() {
    events_ |= KWriteEvent;
    update();
  }
  void disableWriting() {
    events_ &= ~KWriteEvent;
    update();
  }
  void disableAll() {
    events_ = kNoneEvent;
    update();
  }
  bool isWriting() const { return events_ & KWriteEvent; }
  bool isReading() const { return events_ & KReadEvent; }

  // 对于EPollPoller来说, index_是指明Channel的状态
  // 是新的,还是已经添加过的,还是已经删除的
  int index() const { return index_; }
  void setIndex(int index) { index_ = index; }

  // for debug
  std::string reventsToString() const;
  std::string eventsToString() const;

  EventLoop *ownerLoop() { return loop_; }

  void remove();

private:
  static std::string eventsToString(int fd, int ev);
  void update();
  void handleEventWithGuard(Timestamp receiveTime);

private:
  // 变量前加一个k代表是常量
  static const int kNoneEvent;
  static const int KReadEvent;
  static const int KWriteEvent;

  EventLoop *loop_; // 指明Channel所属的事件循环EventLoop对象
  const int fd_;    // Channel对应的fd, 但是Channel不负责close()
  int events_;      // 指明Channel对应的fd的关心事件
  int revents_;     // 指明Channel对应的fd的就绪事件
  int index_;       // 指明Channel的状态

  std::weak_ptr<void> tie_; // 绑定的对象
  bool tied_;               // Todo: 和TcpConnection有关
  bool eventHandling_; // 是否正在处理事件（是否正在调用handleEvent）
  bool addedToLoop_; // channel是否已经添加到某个EventLoop对象

  // 对应书中的四个（三个半）事件的回调函数
  ReadEventCallback readCallback_;
  EventCallback writeCallback_;
  EventCallback errorCallback_;
  EventCallback closeCallback_;

}; // end of class Channel

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_CHANNEL_H
