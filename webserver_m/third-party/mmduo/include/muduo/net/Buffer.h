#ifndef MUDUO_NET_BUFFER_H
#define MUDUO_NET_BUFFER_H

#include "muduo/base/copyable.h"
#include "muduo/base/StringPiece.h"
#include "muduo/base/Types.h"
#include "muduo/base/copyable.h"

#include "muduo/net/Endian.h"

#include <algorithm>
#include <vector>

#include <assert.h>
#include <string.h>

namespace muduo {
namespace net {

/**
 * Buffer是线程不安全的（没有加任何锁）
 * 因为muduo中Buffer主要用于TcpConnection中，
 * 而TcpConnection中对于Buffer的操作始终都是在同一个线程进行，
 * 所以没必要设计为线程安全的（降低性能）
 */
class Buffer : public copyable {
public:
  static const int kCheapPrepend = 8;
  // Buffer前头空出8B，使得往Buffer前面增加数据更加容易（通常增加的是数据长度）
  static const int kInitialSize = 1024; // Buffer初始长度

  explicit Buffer(size_t initialSize = kInitialSize)
      : buffer_(kInitialSize + kCheapPrepend), readIndex_(kCheapPrepend),
        writeIndex_(kCheapPrepend) {}

  // 交换两个Buffer（主要交换是vector和读写下标）
  void swap(Buffer &rhs) {
    buffer_.swap(rhs.buffer_);
    std::swap(readIndex_, rhs.readIndex_);
    std::swap(writeIndex_, rhs.writeIndex_);
  }

  // 返回可读数据的字节数
  size_t readableBytes() const { return writeIndex_ - readIndex_; }

  // 返回可写数据的字节数
  size_t writeableBytes() const { return buffer_.size() - writeIndex_; }

  // 返回Buffer中已经读取的数据的字节数（这部空间还可以利用）
  size_t prependableBytes() const { return readIndex_; }

  // 可读数据的开头
  const char *peek() const { return begin() + readIndex_; }

  // 可写区域的开头
  char *beginWrite() { return begin() + writeIndex_; }

  // 可写区域的开头
  const char *beginWrite() const { return begin() + writeIndex_; }

  // 在可读数据中返回 /r/n/ 的位置
  const char *findCRLF() const {
    const char *crlf = std::search(peek(), beginWrite(), kCRLF, kCRLF + 2);
    return crlf == beginWrite() ? NULL : crlf;
  }

  // 从peek()开始，返回 EOL（end of line， 也即是 \n） 的位置
  const char *findEOL() const {
    const void *eol = memchr(peek(), '\n', readableBytes());
    return static_cast<const char *>(eol);
  }

  // 从start开始，返回 EOL（end of line， 也即是 \n） 的位置
  const char *findEOL(const char *start) const {
    // 检测start位置是否位于可读区间
    assert(peek() <= start && start <= beginWrite());
    const void *eol = memchr(start, '\n', beginWrite() - start);
    return static_cast<const char *>(eol);
  }

  // 移动读指针（读数据后调用）
  void retrieve(size_t len) {
    assert(len <= readableBytes());
    if (len < readableBytes()) {
      readIndex_ += len;
    } else {
      retrieveAll();
    }
  }

  void retrieveAll() {
    readIndex_ = kCheapPrepend;
    writeIndex_ = kCheapPrepend;
  }

  void retrieveUntil(const char *end) {
    assert(peek() <= end && end <= beginWrite());
    retrieve(end - peek());
  }

  // 将部分可读数据转换为string
  std::string retrieveAsString(size_t len) {
    assert(len <= readableBytes());
    std::string res(peek(), len);
    retrieve(len);
    return res;
  }

  // 将所有可读数据转换为string
  std::string retrieveAllAsString() {
    return retrieveAsString(readableBytes());
  }

  StringPiece toStringPiece() const {
    return StringPiece(peek(), static_cast<int>(readableBytes()));
  }

  // 在Buffer尾部添加数据
  void append(const StringPiece &str) { append(str.data(), str.size()); }

  void append(const void *data, size_t len) {
    append(static_cast<const char *>(data), len);
  }

  void append(const char *data, size_t len) {
    ensureWritableBytes(len);
    std::copy(data, data + len, beginWrite());
    hasWritten(len);
  }

  // 确保writeableBytes() >= len
  void ensureWritableBytes(size_t len) {
    if (writeableBytes() < len) {
      makeSpace(len);
    }
    assert(writeableBytes() >= len);
  }

  // 移动写指针（写数据后调用）
  void hasWritten(size_t len) {
    assert(len <= writeableBytes());
    writeIndex_ += len;
  }

  // 移动写指针（撤销所写数据调用）
  void unwrite(size_t len) {
    assert(len <= readableBytes());
    writeIndex_ -= len;
  }

  // 以下所有的函数都是为了添加或者读取intxx_t
  void retrieveInt64() { retrieve(sizeof(int64_t)); }

  void retrieveInt32() { retrieve(sizeof(int32_t)); }

  void retrieveInt16() { retrieve(sizeof(int16_t)); }

  void retrieveInt8() { retrieve(sizeof(int8_t)); }

  void appendInt64(int64_t x) {
    int64_t be64 = sockets::hostToNetwork64(x);
    append(&be64, sizeof be64);
  }

  void appendInt32(int32_t x) {
    int32_t be32 = sockets::hostToNetwork32(x);
    append(&be32, sizeof be32);
  }

  void appendInt16(int16_t x) {
    int16_t be16 = sockets::hostToNetwork16(x);
    append(&be16, sizeof be16);
  }

  void appendInt8(int8_t x) { append(&x, sizeof x); }

  int64_t readInt64() {
    int64_t result = peekInt64();
    retrieveInt64();
    return result;
  }

  int32_t readInt32() {
    int32_t result = peekInt32();
    retrieveInt32();
    return result;
  }

  int16_t readInt16() {
    int16_t result = peekInt16();
    retrieveInt16();
    return result;
  }

  int8_t readInt8() {
    int8_t result = peekInt8();
    retrieveInt8();
    return result;
  }

  int64_t peekInt64() const {
    assert(readableBytes() >= sizeof(int64_t));
    int64_t be64 = 0;
    ::memcpy(&be64, peek(), sizeof be64);
    return sockets::networkToHost64(be64);
  }

  int32_t peekInt32() const {
    assert(readableBytes() >= sizeof(int32_t));
    int32_t be32 = 0;
    ::memcpy(&be32, peek(), sizeof be32);
    return sockets::networkToHost32(be32);
  }

  int16_t peekInt16() const {
    assert(readableBytes() >= sizeof(int16_t));
    int16_t be16 = 0;
    ::memcpy(&be16, peek(), sizeof be16);
    return sockets::networkToHost16(be16);
  }

  int8_t peekInt8() const {
    assert(readableBytes() >= sizeof(int8_t));
    int8_t x = *peek();
    return x;
  }

  void prependInt64(int64_t x) {
    int64_t be64 = sockets::hostToNetwork64(x);
    prepend(&be64, sizeof be64);
  }

  void prependInt32(int32_t x) {
    int32_t be32 = sockets::hostToNetwork32(x);
    prepend(&be32, sizeof be32);
  }

  void prependInt16(int16_t x) {
    int16_t be16 = sockets::hostToNetwork16(x);
    prepend(&be16, sizeof be16);
  }

  void prependInt8(int8_t x) { prepend(&x, sizeof x); }

  void prepend(const void *data, size_t len) {
    assert(len <= prependableBytes());
    readIndex_ -= len;
    const char *d = static_cast<const char *>(data);
    std::copy(d, d + len, begin() + readIndex_);
  }

  void shrink(size_t reserve) {
    // FIXME: use vector::shrink_to_fit() in C++ 11 if possible.
    // Buffer other;
    // other.ensureWritableBytes(readableBytes()+reserve);
    // other.append(toStringPiece());
    // swap(other);

    buffer_.shrink_to_fit();
  }

  size_t internalCapacity() const { return buffer_.capacity(); }

  ssize_t readFd(int fd, int *savedErrno);

private:
  char *begin() { return buffer_.data(); }

  const char *begin() const { return buffer_.data(); }

  // 使得writeableBytes等于len
  void makeSpace(size_t len) {
    if (writeableBytes() + prependableBytes() < len + kCheapPrepend) {
      buffer_.resize(writeIndex_ + len);
    } else {
      assert(kCheapPrepend < readIndex_);
      size_t readable = readableBytes();
      std::copy(begin() + readIndex_, begin() + writeIndex_,
                begin() + kCheapPrepend);
      readIndex_ = kCheapPrepend;
      writeIndex_ = readIndex_ + readable;
      assert(readable == readableBytes());
    }
  }

private:
  std::vector<char> buffer_;
  size_t readIndex_;
  size_t writeIndex_;

  // 就是 /r/n
  static const char kCRLF[];
}; // end of class Buffer

}; // end of namespace net
}; // end of namespace muduo

#endif // MUDUO_NET_BUFFER_H