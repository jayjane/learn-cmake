#ifndef MUDUO_NET_TIMERID
#define MUDUO_NET_TIMERID

#include "muduo/base/copyable.h"

namespace muduo {

namespace net {

class Timer;

class TimerId : public copyable {
public:
  TimerId() : timer_(NULL), sequence_(0) {}

  TimerId(Timer *timer, int64_t seq) : timer_(timer), sequence_(seq) {}

  ~TimerId() = default;

  friend class TimerQueue;

private:
  Timer *timer_;     // 对应定时器的指针
  int64_t sequence_; // 定时器的序号
};                   // end of class TimerId

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_TIMERID
