#ifndef MUDUO_NET_EVENTLOOPTHREAD_H
#define MUDUO_NET_EVENTLOOPTHREAD_H

#include "muduo/base/Thread.h"

#include <condition_variable>
#include <mutex>

namespace muduo {

namespace net {

class EventLoop;

class EventLoopThread : public noncopyable {
public:
  using ThreadInitCallback = std::function<void(EventLoop *)>;

  EventLoopThread(const ThreadInitCallback &cb = ThreadInitCallback(),
                  const std::string &name = std::string());
  ~EventLoopThread();
  EventLoop *startLoop();

private:
  void threadFunc(); // 线程函数

  EventLoop *loop_; // IO线程所拥有的EventLoop对象
  bool exiting_;    // IO线程退出标志
  Thread thread_;   // IO线程对应的Thread类
  std::mutex mtx_;
  std::condition_variable cond_;
  ThreadInitCallback callback_; // 线程初始化函数

}; // end of class EventLoop

}; // end of namespace net

}; // end of namespace muduo

#endif // !MUDUO_NET_EVENTLOOPTHREAD_H
