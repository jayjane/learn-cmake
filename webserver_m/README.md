# 简介
基于网络库mmuduo(一个模仿muduo库写的网络库或者说mini版的muduo)写的一个Web Server。

# 依赖
boost
* yum install boost -y
* yum install boost-devel --allowerasing

mmuduo

jsoncpp
* yum install jsoncpp.x86_64
* yum install jsoncpp.devel.x86_64

GTest
* yum install gtest gtest-devel

# 安装运行

# 性能测试

# 文档

# 代码规范
尽量遵守Google C++ style

# 功能
支持HTML、JavaScript、CSS、图片、视频文件访问
支持用户登录、注册功能

# 特点
* 支持HTTP请求中x-www-form-urlencoded和json解码
* 应用技术：数据库连接池

# Todo
* 23/9/19，done：数据库连接池、数据库链接池单元测试、json解析单元测试
* 封装Redis
* 内存池